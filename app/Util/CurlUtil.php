<?php

namespace App\Util;

class CurlUtil
{
    /**
     * curl
     * @param $url
     * @param $data
     * @return mixed
     */
    public static function curl($url, $data)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        $info = curl_getinfo($ch);
        $last_error = curl_error($ch);
        curl_close($ch);

        return json_decode($result);
    }

    /**
     * get 请求
     */
    public static function get($url, $data, $header)
    {
        $curl = curl_init();
        //设置抓取的url
        curl_setopt($curl, CURLOPT_URL, $url . '?' . http_build_query($data));
        //设置头文件的信息作为数据流输出
        curl_setopt($curl, CURLOPT_HEADER, 0);
        // 超时设置,以秒为单位
        curl_setopt($curl, CURLOPT_TIMEOUT, 1);

        // 超时设置，以毫秒为单位
        // curl_setopt($curl, CURLOPT_TIMEOUT_MS, 500);

        // 设置请求头
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        //设置获取的信息以文件流的形式返回，而不是直接输出。
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        //执行命令
        $data = curl_exec($curl);

        // 显示错误信息
        if (curl_error($curl)) {
            throw new \Exception(curl_error($curl));
        } else {
            // 打印返回的内容

            curl_close($curl);
            return $data;
        }

    }

    /**
     * curl -f
     * @param $url
     * @param $file_path
     * @return bool|string
     */
    public static function curlF($url, $file_path)
    {
        $ch = curl_init($url);

        // 获取CURLFile实例
        $xlsCurlFile = self::makeCurlFile($file_path);

        $data = array('data' => $xlsCurlFile);

        // TRUE 时会发送 POST 请求，类型为：application/x-www-form-urlencoded，是 HTML 表单提交时最常见的一种。
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        // 从 PHP 5.5.0 开始, @ 前缀已被废弃，文件可通过 CURLFile 发送。 设置 CURLOPT_SAFE_UPLOAD 为 TRUE 可禁用 @ 前缀发送文件，以增加安全性。
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        // 执行给定的 cURL 会话
        // 成功时返回 TRUE， 或者在失败时返回 FALSE。 然而，如果 设置了 CURLOPT_RETURNTRANSFER 选项，函数执行成功时会返回执行的结果，失败时返回 FALSE 。
        $result = curl_exec($ch);

        if (curl_errno($ch)) {// 返回错误代码或在没有错误发生时返回 0 (零)。
            // 返回错误信息，或者如果没有任何错误发生就返回 '' (空字符串)。
            $result = curl_error($ch);
        }

        // 关闭 cURL 会话
        curl_close($ch);
        return $result;
    }

    /**
     * 根据文件目录将文件转为curlfile对象
     * @param $file_path
     * @return \CURLFile
     */
    private static function makeCurlFile($file_path)
    {
        $info = pathinfo($file_path);
        $name = $info['basename'];
        $fi = new \finfo(FILEINFO_MIME_TYPE);
        $mime = $fi->file($file_path);
        return new \CURLFile($file_path, $mime, $name);
    }

    public static function request($path, $head = [], $body = [])
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $path);
        $header = $head;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        $jsonStr = $body ? json_encode($body) : ''; //转换为json格式
        curl_setopt($ch, CURLOPT_POST, 1);
        if ($jsonStr) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonStr);
        }
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);    // 信任任何证
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close($ch);
        return json_decode($response, true);
    }

}
