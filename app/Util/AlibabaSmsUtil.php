<?php

namespace App\Util;

use AlibabaCloud\Client\AlibabaCloud;
use AlibabaCloud\Client\Exception\ClientException;
use AlibabaCloud\Client\Exception\ServerException;

class AlibabaSmsUtil
{
    private static function getAlibabaCloud()
    {
        return AlibabaCloud::accessKeyClient(env('ALIBABA_ACCESS_KEY'), env('ALIBABA_ACCESS_SECRET'))
            ->regionId('cn-hangzhou')
            ->asDefaultClient();
    }

    /**
     * 身份验证验证码
     * @param $phone
     * @param $code
     */
    public static function getValidationCode($phone,$code)
    {
        $config = [
            'phone' => $phone,
            'template_code' => env('ALIBABA_SMS_CODE'),
            'code' => [
                'code' => $code
            ]
        ];
        self::sendSms($config);
    }

    /**
     * 登录确认验证码
     * @param $phone
     * @param $code
     */
    public static function getLoginCode($phone,$code)
    {
        $config = [
            'phone' => $phone,
            'template_code' => env('ALIBABA_SMS_CODE'),
            'code' => [
                'code' => $code
            ]
        ];
        self::sendSms($config);
    }

    /**
     * 登录异常验证码
     * @param $phone
     * @param $code
     */
    public static function getAbnormalLoginCode($phone,$code)
    {
        $config = [
            'phone' => $phone,
            'template_code' => env('ALIBABA_SMS_CODE'),
            'code' => [
                'code' => $code
            ]
        ];
        self::sendSms($config);
    }

    /**
     * 用户注册验证码
     * @param $phone
     * @param $code
     */
    public static function getRegisterCode($phone,$code)
    {
        $config = [
            'phone' => $phone,
            'template_code' => env('ALIBABA_SMS_CODE'),
            'code' => [
                'code' => $code
            ]
        ];
        self::sendSms($config);
    }

    /**
     * 修改密码验证码
     * @param $phone
     * @param $code
     */
    public static function getSetPasswordCode($phone,$code)
    {

        $config = [
            'phone' => $phone,
            'template_code' => env('ALIBABA_SMS_CODE'),
            'code' => [
                'code' => $code
            ]
        ];
        self::sendSms($config);
    }

    /**
     * 信息变更验证码
     * @param $phone
     * @param $code
     */
    public static function getInformationChangeCode($phone,$code)
    {
        $config = [
            'phone' => $phone,
            'template_code' => env('ALIBABA_SMS_CODE'),
            'code' => [
                'code' => $code
            ]
        ];
        self::sendSms($config);
    }

    /**
     * @throws ClientException
     * @throws ServerException
     */
    public static function sendSms($config)
    {
        self::getAlibabaCloud();
        try {
            $result = AlibabaCloud::rpc()
                ->product('Dysmsapi')
                // ->scheme('https') // https | http
                ->version('2017-05-25')
                ->action('SendSms')
                ->method('POST')
                ->host('dysmsapi.aliyuncs.com')
                ->options([
                    'query' => [
                        'RegionId' => "cn-hangzhou",
                        'PhoneNumbers' => $config['phone'],
                        'SignName' => env('ALIBABA_SIGN_NAME'),
                        'TemplateCode' => $config['template_code'],
                        'TemplateParam' => json_encode($config['code']),
                    ],
                ])
                ->request();
            $result = $result->toArray();
            if( $result['Message'] == 'OK'){
               return $result;
            }
        } catch (ClientException|ServerException $e) {
            throw $e;
        }
    }

}
