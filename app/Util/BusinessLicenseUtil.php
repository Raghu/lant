<?php

namespace App\Util;

class BusinessLicenseUtil
{
    public static function query($url)
    {
        $host = "http://blicence.market.alicloudapi.com";
        $path = "/ai_business_license";
        $method = "POST";
        //阿里云APPCODE
        $appcode = "547c7332f63849c486c53924d16ecbb5";
        $headers = array();
        array_push($headers, "Authorization:APPCODE " . $appcode);
        //根据API的要求，定义相对应的Content-Type
        array_push($headers, "Content-Type".":"."application/x-www-form-urlencoded; charset=UTF-8");
        #IMAGE_TYPE内容数据类型，如：0，则表示BASE64编码；1，则表示图像文件URL链接
        #启用BASE64编码方式进行识别
        #内容数据类型是BASE64编码
        //$file = "图像文件";
        //$AI_BUSINESS_LICENSE_IMAGE = base64_encode(file_get_contents($file));
        //$AI_BUSINESS_LICENSE_IMAGE = urlencode($AI_BUSINESS_LICENSE_IMAGE);
        //$AI_BUSINESS_LICENSE_IMAGE_TYPE = "0";
        #启用URL方式进行识别
        #内容数据类型是图像文件URL链接
        $AI_BUSINESS_LICENSE_IMAGE = $url;
        $AI_BUSINESS_LICENSE_IMAGE = urlencode($AI_BUSINESS_LICENSE_IMAGE);
        $AI_BUSINESS_LICENSE_IMAGE_TYPE = "1";
        $bodys = "AI_BUSINESS_LICENSE_IMAGE=".$AI_BUSINESS_LICENSE_IMAGE."&AI_BUSINESS_LICENSE_IMAGE_TYPE=".$AI_BUSINESS_LICENSE_IMAGE_TYPE;
        $url = $host . $path;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_FAILONERROR, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        //curl_setopt($curl, CURLOPT_HEADER, true);
        if (1 == strpos("$".$host, "https://"))
        {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        }
        curl_setopt($curl, CURLOPT_POSTFIELDS, $bodys);
        return json_decode(curl_exec($curl), true);
    }
}
