<?php
/**
 * Created by Raghu Liu
 * Date: 2022/9/18
 * Time: 22:19
 */

namespace App\Core;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class BaseModel extends Model
{
    public const CREATED_AT = 'created_at';
    public const UPDATED_AT = NULL;

    protected $guarded = [];

    protected function serializeDate(\DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
