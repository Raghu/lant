<?php
/**
 * Created by Raghu Liu
 * Date: 2023/9/23
 * Time: 10:44
 */

namespace App\Core;

abstract class BaseCache
{
    public static $name;

    public static $desc;

    public static $type;

    abstract static function allKeys();
}
