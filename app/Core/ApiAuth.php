<?php

namespace App\Core;

class ApiAuth
{

    private static $user;

    private function __construct()
    {
    }

    private function __clone()
    {

    }

    public static function id()
    {
        $user = auth()->user();
        return $user->user_id;
    }

    public static function getUser()
    {
        return auth()->user();
    }

    public static function clean()
    {
        self::$user = null;
    }
}
