<?php

namespace App\Core;


use App\Models\Admin;

class AdminActionName
{
    private static $data;

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    public static function setData($data)
    {
        return self::$data = $data;
    }

    public static function getData()
    {
        return self::$data;
    }
}
