<?php
/**
 * Created by Raghu Liu
 * Date: 2022/9/17
 * Time: 22:54
 */

namespace App\Core;

use Illuminate\Pagination\LengthAwarePaginator;

class Pagination extends LengthAwarePaginator
{
    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'data' => $this->items->toArray(),
            'pagination' => [
                'page' => $this->currentPage(),
                'pageSize' => $this->perPage(),
                'lastPage' => $this->lastPage(),
                'total' => $this->total()
            ]
        ];
    }
}
