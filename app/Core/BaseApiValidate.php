<?php

namespace App\Core;

class BaseApiValidate
{
    public function rule($rule, $message = [])
    {
        return [
            'rule' => $rule,
            'message' => $message
        ];
    }
}
