<?php
/**
 * Created by PhpStorm.
 * User: Lenovo
 * Date: 2020/6/11
 * Time: 10:57
 */

namespace App\Core;

use App\Exceptions\ApiException;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;

class BaseApiController extends Controller
{

    private $app;
    public $apiValidate;

    public function __construct()
    {
        $this->app = app();

        $this->middleware(function ($request, \Closure $next) {
            $this->makeApiValidate();
            $this->apiParameterValidate();
            return $next($request);

        });
    }

    public function makeApiValidate()
    {
        //判断验证是否存在
        $class_name = str_replace('App\Http\Controllers\Api\\', '', get_class($this)) . 'Validate';

        $validate_class = 'App\Http\Controllers\Api\Validate\\' . $class_name;

        if (class_exists($validate_class)) {
            $apiValidate = $this->app->make($validate_class);
            return $this->apiValidate = $apiValidate;
        }
    }

    /**
     * 控制器参数验证
     */
    public function apiParameterValidate()
    {
        $actionName = request()->route()->getActionName();
        $actionName = explode('@', $actionName);
        $actionName = $actionName[1];

        $apiValidate = $this->apiValidate;

        if ($apiValidate) {
            if (method_exists($apiValidate, $actionName)) {
                $data = \request()->all();
                $rule = $apiValidate->$actionName()['rule'];
                $message = $apiValidate->$actionName()['message'];
                $validator = \Validator::make($data, $rule, $message);
                if (!$validator->passes()) {
                    $msg = $validator->errors()->first();
                    throw new ApiException($msg);
                }

            }

        }
    }

    /**
     * 成功返回
     * @param $msg
     * @param $data
     * @return array
     */
    protected function success($data = [], $msg = 'ok', $status = 200)
    {
        return [
            'status' => $status,
            'msg' => $msg,
            'result' => $data
        ];
    }

    /**
     * 失败返回
     * @param $msg
     * @return array
     */
    public function error($msg = 'error', $code = 400)
    {
        return [
            'status' => $code,
            'msg' => $msg
        ];
    }


    /**
     * 处理分页数据
     * @param $list
     * @return array
     */
    public function page($list)
    {
        $list = is_array($list) ? $list : $list->toArray();
        $current_page = $list['current_page']; //当前页
        $last_page = $list['last_page'];       //最后页
        $total = $list['total'];               //总量
        $next_page = 0;                        //下一页
        $prev_page = 0;                        //上一页
        $per_page = $list['per_page'];                         //每页行数
        $is_last_page = 0;
        $data = $list['data'];
        $data = is_array($data) ? $data : $data->toArray();
        if ($current_page != $last_page) {
            $next_page = $current_page + 1;
        } else {
            $next_page = null;
            $is_last_page = 1;
        }

        if ($current_page != 1) {
            $prev_page = $current_page - 1;
        } else {
            $prev_page = null;
        }


        return [
            'pagination' => [
                'current_page' => $current_page,
                'last_page' => $last_page,
                'total' => $total,
                'next_page' => $next_page,
                'prev_page' => $prev_page,
                'page_size' => (int)$per_page,
                'is_last_page' => $is_last_page,
            ],
            'items' => $data,
        ];
    }

    /**
     * 处理分页数据
     * @param $list
     * @return array
     */
    public function antpage($list)
    {
        $list = is_array($list) ? $list : $list->toArray();
        $current_page = $list['current_page']; //当前页
        $last_page = $list['last_page'];       //最后页
        $total = $list['total'];               //总量
        $next_page = 0;                        //下一页
        $prev_page = 0;                        //上一页
        $per_page = $list['per_page'];                         //每页行数
        $is_last_page = 0;
        $data = is_array($list['data']) ? $list['data'] : $list['data']->toArray();
        $data = array_values($data);
        if ($current_page != $last_page) {
            $next_page = $current_page + 1;
        } else {
            $next_page = null;
            $is_last_page = 1;
        }

        if ($current_page != 1) {
            $prev_page = $current_page - 1;
        } else {
            $prev_page = null;
        }


        return [
            'pagination' => [
                'current_page' => $current_page,
                'last_page' => $last_page,
                'total' => $total,
                'next_page' => $next_page,
                'prev_page' => $prev_page,
                'page_size' => (int)$per_page,
                'is_last_page' => $is_last_page,
            ],
            'items' => $data,
        ];
    }

    /**
     * 制作验证码
     */
    public function makeCode()
    {
        return str_pad(random_int(1, 9999), 4, 0, STR_PAD_LEFT);//随机数
    }

    //控制器中
    public function makePage($array, $page, $page_size)
    {
        //当前页数 默认1
        $page = $page ?: 1;
        //每页的条数
        $perPage = $page_size;
        //计算每页分页的初始位置
        $offset = ($page * $perPage) - $perPage;
        return new LengthAwarePaginator(array_slice($array, $offset, $perPage, true), count($array), $perPage, $page, []);
    }

}
