<?php

namespace App\Core;


use Illuminate\Support\Facades\Redis;

class RedisCache
{
    private static $redis;

    private function __construct()
    {
    }

    public static function redisInit()
    {
        if (self::$redis) {
            return self::$redis;
        } else {
            self::$redis = app('redis.connection');
            return self::$redis;
        }
    }

    private function __clone()
    {
    }

    public static function getKeys($pattern)
    {
        $prefix = config('database.redis.options.prefix');
        $redis  = self::redisInit();
        $keys   = $redis->keys($pattern);

        foreach ($keys as $key => $value) {
            $keys[$key] = str_replace($prefix, '', $value);
        }

        return $keys;
    }

    public static function getInfo($key)
    {
        $size  = self::strlen($key);
        $value = self::get($key);
        $ttl   = self::ttl($key);
        return [
            'size'  => $size,
            'value' => $value,
            'ttl'   => $ttl,
        ];
    }

    public static function __callStatic($function, $parameter)
    {
        $redis = self::redisInit();
        return $redis->$function(...$parameter);
    }
}
