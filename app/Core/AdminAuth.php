<?php

namespace App\Core;


use App\Models\Admin;

class AdminAuth
{
    private function __construct()
    {
    }

    private function __clone()
    {

    }

    public static function id()
    {
        $admin = auth()->user();
        return $admin->admin_id;
    }

    public static function getAdmin()
    {
        return auth()->user();
    }
}
