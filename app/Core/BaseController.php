<?php
/**
 * Created by PhpStorm.
 * User: Lenovo
 * Date: 2020/6/11
 * Time: 10:57
 */

namespace App\Core;


use App\Exceptions\ApiException;
use App\Http\Controllers\Controller;
use App\Service\LogService;
use Spatie\Activitylog\Models\Activity;

class BaseController extends Controller
{
    /**
     * 成功返回
     * @param $msg
     * @param $data
     * @return array
     */
    protected function success($data = [], $msg = 'ok')
    {
        return [
            'status' => 200,
            'msg'    => $msg,
            'result' => $data
        ];
    }

    /**
     * 失败返回
     * @param $msg
     * @return array
     */
    public function error($msg = '', $code = 400)
    {
        return [
            'status' => $code,
            'msg'    => $msg
        ];
    }
}
