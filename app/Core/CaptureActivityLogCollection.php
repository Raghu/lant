<?php
/**
 * Created by Raghu Liu
 * Date: 2023/9/23
 * Time: 19:57
 */

namespace App\Core;

class CaptureActivityLogCollection
{
    private static $collection;

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    public static function push($item)
    {
        if (!self::$collection) {
            self::$collection = collect();
        }
        self::$collection->push($item);
    }

    public static function getCollection()
    {
        return self::$collection;
    }

    public static function clear()
    {
        self::$collection = null;
    }
}
