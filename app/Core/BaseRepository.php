<?php

namespace App\Core;

use App\Exceptions\ApiException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

abstract class BaseRepository
{

    public $model;

    private $app;

    /**
     * @var array 查询搜索参数
     */
    public $search_param;

    /**
     * @var int 查询分页行数
     */
    protected $pageSize = 10;

    /**
     * @var array 查询排序规则
     */
    protected $sorter = [];

    public function __construct()
    {
        $this->app = app();
        $this->makeModel();
        $this->setSearchParam([]);
    }

    abstract public function model();

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Exception
     */
    public function makeModel()
    {
        $model = $this->app->make($this->model());

        if (!$model instanceof Model) {
            throw new \Exception(get_class($model) . ' Repository model error');
        }

        return $this->model = $model;
    }

    /**
     * @throws ApiException
     */
    public function create($data)
    {
        return $this->model->create($data);
    }

    /**
     * 筛选增
     * @param array $field
     * @param $data
     * @return mixed
     * @throws ApiException
     */
    public function allowFieldCreate(array $field, $data)
    {
        if (!$data) {
            $data = [];
        }
        //将$field键值互换后取两数组键交集
        $data = array_intersect_key($data, array_flip($field));
        return $this->create($data);
    }

    /**
     * 排除增
     * @param array $field
     * @param $data
     * @return mixed
     * @throws ApiException
     */
    public function exceptFieldCreate(array $field, $data)
    {
        if (!$data) {
            $data = [];
        }
        $modelField = Schema::getColumnListing($this->model->table);

        $key = array_diff($modelField, $field);

        $data = array_intersect_key($data, array_flip($key));

        return $this->create($data);
    }

    /**
     * 排除改
     * @param array $field
     * @param $data
     * @return mixed
     * @throws ApiException
     */
    public function exceptFieldUpdate(array $field, $data, $id)
    {
        if (!$data) {
            $data = [];
        }
        $modelField = Schema::getColumnListing($this->model->table);

        $key = array_diff($modelField, $field);

        $data = array_intersect_key($data, array_flip($key));

        return $this->update($data, $id);
    }

    /**
     * 筛选改
     * @param array $field
     * @param $data
     * @return mixed
     * @throws ApiException
     */
    public function allowFieldUpdate(array $field, $data, $id)
    {
        if (!$data) {
            $data = [];
        }
        //将$field键值互换后取两数组键交集
        $data = array_intersect_key($data, array_flip($field));
        return $this->update($data, $id);
    }

    //改
    public function update($data, $id)
    {
        $model = $this->model->find($id);

        $model->update($data);

        return $model;
    }

    //批量改
    public function updateWhere($data, $where)
    {
        $model = $this->model->where($where);
        return $model->update($data);
    }

    //删除
    public function delete($id)
    {
        $model = $this->model->find($id);
        if ($model) {
            return $this->model->find($id)->delete();
        } else {
            throw new ApiException('数据不存在');
        }

    }

    /**
     * 不存在则创建 存在则修改
     * @throws ApiException
     * @throws \Exception
     */
    public function updateOrCreate($data, $where)
    {
        if (!is_array($data)) {
            throw new \Exception('The first parameter must be an array');
        }
        if (!is_array($where)) {
            throw new \Exception('The secound parameter must be an array');
        }

        $model = $this->model->where($where)->count();

        if ($model > 0) {
            $this->updateWhere($data, $where);
        } else {
            $this->create($data);
        }

    }

    //批量删除
    public function deleteWhere($where)
    {
        if (!is_array($where)) {
            throw new \Exception('The first parameter must be an array');
        }
        return $this->model->where($where)->delete();
    }

    //判断是否存在
    public function isExist($id, $where = [], $msg = null)
    {
        if (!$this->model->where($where)->find($id)) {
            throw new ApiException($msg ? $msg : '没有找到该信息');
        }
        return true;
    }

    //根据id查询
    public function find($id)
    {
        return $this->model->find($id);
    }

    // 建立搜索参数
    public function setSearchParam($data)
    {
        $this->searchParam = new RepositorySearch($data);
        return $this;
    }

    public function get()
    {
        return $this->model->get();
    }

    /**
     * 动态修改更新表单验证规则
     * @param $key
     * @param $value
     * @return mixed
     */
    public function setUpdateRule($key, $value)
    {
        $this->updateRule[$key] = $value;
        return $this;
    }

    /**
     * 动态修改新增表单验证规则
     * @param $key
     * @param $value
     * @return $this
     */
    public function setCreateRule($key, $value)
    {
        $this->createRule[$key] = $value;
        return $this;
    }

    public function setCreateMessage($key, $value)
    {
        $this->createMessage[$key] = $value;
        return $this;
    }

    public function setUpdateMessage($key, $value)
    {
        $this->updateMessage[$key] = $value;
        return $this;
    }

    public function setByRequest()
    {
        $request = request();
        $this->setSearchParam($request->input('search', []));
        $this->setPageSize($request->input('pageSize'));
        $this->setSort($request->input('sorter', []));
        return $this;
    }

    public function setPageSize($page_size)
    {
        $this->pageSize = $page_size;
        return $this;
    }

    public function setSort(array $sorter)
    {
        foreach ($sorter as $key => $value) {
            $sorter[$key]['orderBy'] = Str::snake($value['orderBy']);
        }
        $this->sorter = $sorter;
        return $this;
    }

    public function query($model)
    {
        $sorter = $this->sorter;
        foreach ($sorter as $item) {
            if (!isset($item['orderType'])) continue;
            $model = $model->orderBy($item['orderBy'], $item['orderType']);
        }

        if ($this->pageSize) return $model->paginate($this->pageSize);
        return $model->get();
    }
}
