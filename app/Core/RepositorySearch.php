<?php

namespace App\Core;

class RepositorySearch
{
    /**
     * @throws \Exception
     */
    public function __construct($search)
    {
        if ($search !== null) {
            if (!is_array($search)) {
                throw new \Exception('Core:The first argument should be an array');
            }
            foreach ($search as $key => $value) {
                $this->$key = $value;
            }
        }
    }

    public function __get($property)
    {
        return null;
    }
}
