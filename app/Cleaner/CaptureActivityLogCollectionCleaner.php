<?php
/**
 * Created by Raghu Liu
 * Date: 2023/9/23
 * Time: 22:34
 */

namespace App\Cleaner;

use Hhxsv5\LaravelS\Illuminate\Cleaners\BaseCleaner;

class CaptureActivityLogCollectionCleaner extends BaseCleaner
{
    public function clean()
    {
        \App\Core\CaptureActivityLogCollection::clear();
    }
}
