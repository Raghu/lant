<?php

namespace App\Http\Middleware;

use App\Exceptions\ApiException;
use Closure;

class ApiValidateMiddleware
{
    /**
     * @throws ApiException
     */
    public function handle($request, Closure $next, $guard = null)
    {
        /* 验证端口参数 */
        $controller          = $request->route()->getAction()['controller'];
        $validator           = str_replace('Controllers', 'Validator', $controller);
        $validator           = str_replace('Controller', 'Validator', $validator);
        $validatorController = explode('@', $validator)[0];
        $validatorFunction   = explode('@', $validator)[1];

        if (method_exists($validatorController, $validatorFunction)) {
            $validatorData = call_user_func($validatorController . '::' . $validatorFunction);
            $validator     = \Validator::make($request->all(), $validatorData['rule'], isset($validatorData['message']) ? $validatorData['message'] : []);
            if (!$validator->passes()) {
                $errorMessages = $validator->errors()->getMessages();
                if ($guard === 'api') {
                    throw new ApiException($validator->errors()->first());
                } else {
                    $errorMessages = $validator->errors()->getMessages();

                    $msgs = [];

                    foreach ($errorMessages as $field => $msg) {
                        $msgs[] = ['field' => $field, 'msg' => $msg];
                    }
                    throw new ApiException(json_encode($msgs));
                }
            }
        }

        return $next($request);
    }
}
