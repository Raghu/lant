<?php

namespace App\Http\Middleware;

use App\Core\CaptureActivityLogCollection;
use Closure;
use App\Models\ActivelyLog;

class CaptureActivityLogMiddleware
{
    public function handle($request, Closure $next)
    {
        ActivelyLog::created(function ($activity) {
            CaptureActivityLogCollection::push($activity);
        });

        return $next($request);
    }
}
