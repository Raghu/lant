<?php
/**
 * Created by Raghu Liu
 * Date: 2023/5/13
 * Time: 16:37
 */

namespace App\Http\Middleware;

use App\Exceptions\ApiException;
use App\Models\Admin;
use App\Models\Permission;
use Closure;
use Spatie\Permission\Exceptions\UnauthorizedException;
use Spatie\Permission\PermissionRegistrar;

class PermissionMiddleware
{
    /*
     * 不需要验证权限的路由，业务路由请在config/permission.php中配置。
     *
     * @var \string[][]
     */
    protected $publicPermission = [
        'auth.getuserinfo',
        'auth.bindsocketid',
        'todo.browse',
        'todo.edit',
        'todo.add',
        'log.browse',
        'activelylog.browse',
        'analysis.userbasis',
        'analysis.userbasis',
        'analysis.activeusers',
        'analysis.ordertrend',
        'analysis.financebasis',
        'analysis.rankinglist',
        'analysis.usertrend',
        'analysis.singletrend',
        'my.setinfo',
        'my.getnoticelist',
        'my.readnotice',
        'regionagent.browse'
    ];

    /**
     * 依赖于主业务的工具接口，业务路由请在config/permission.php中配置。
     * @var \string[][]
     */
    protected $mainPermission = [
        'adminrole.getpermission'      => ['adminrole.setpermission'],
        'adminroute.getpermissionlist' => ['adminrole.setpermission'],
        'user.getinvitation'           => ['user.setinvitation'],
        'redis.getkeylist'             => ['redis.browse'],
        'redis.info'                   => ['redis.browse'],
    ];

    public function handle($request, Closure $next)
    {
        $authGuard = app('auth');

        if ($authGuard->guest()) {
            throw UnauthorizedException::notLoggedIn();
        }

        $admin      = $authGuard->user();
        $permission = explode('/', $request->route()->uri);
        $permission = array_slice($permission, -2);
        $permission = strtolower(implode('.', $permission));
        if ($this->checkPermission($admin, $permission)) {
            return $next($request);
        } else {
            throw new ApiException('没有该操作权限，请联系管理员处理。', '403');
        }
    }

    /**
     * @Desc: 判断是否是公共方法
     * @param string $permission
     * @return bool
     * @author: Raghu Liu
     * @Time: 2023/5/14 11:24
     */
    private function isCommon(string $permission): bool
    {
        $publicPermission = array_merge($this->publicPermission, config('permission.public_permission'));
        if (in_array($permission, $publicPermission)) return true;

        $permission = explode('.', $permission);

        if ($permission['0'] == 'common') return true;

        return false;
    }

    /**
     * @Desc: 判断有无接口权限
     * @param string $permission
     * @return bool
     * @author: Raghu Liu
     * @Time: 2023/5/14 11:48
     */
    private function checkPermission(Admin $admin, string $permission): bool
    {
        if ($this->isCommon($permission)) return true;

        $mainPermission = array_merge($this->mainPermission, config('permission.main_permission'));
        /* 如果是工具接口，需要判断该接口的主权限，否则直接判断权限。 */
        if (isset($mainPermission[$permission])) {
            return $admin->hasAnyPermission($mainPermission[$permission]);
        }

        return $admin->hasPermissionTo($permission);
    }
}
