<?php

namespace App\Http\Middleware;

use App\Exceptions\ApiException;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;

class RequestLock
{
    /**
     * Handle an incoming request.
     * @param Request $request
     * @param Closure $next
     * @return mixed
     * @throws ApiException
     */
    public function handle(Request $request, Closure $next)
    {
        $uri = $request->path();
        $token = $request->server('HTTP_ACCESS_TOKEN');
        if (isset($token) && $token != '') {
            $result = Redis::setnx($uri . $token, 1);
            Redis::expire($uri . $token, 3);
            if (!$result) {
                throw new ApiException('处理中 请勿频繁操作');
            }
        }
        return $next($request);
    }
}
