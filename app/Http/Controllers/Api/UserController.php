<?php
/**
 * Created by Raghu Liu
 * Date: 2023/9/12
 * Time: 19:36
 */

namespace App\Http\Controllers\Api;

use App\Core\ApiAuth;
use App\Core\BaseController;
use App\Models\User;
use App\Service\UserAuthService;

class UserController extends BaseController
{
    public function login()
    {
        /**
         * 示例
         */
        $user = User::first();
        $user = UserAuthService::login($user);

        return $this->success([
            'token' => $user->token
        ]);
    }

    public function hellow()
    {
        $user = ApiAuth::getUser();

        return $this->success([
            'hellow' => $user->nickname
        ]);
    }
}
