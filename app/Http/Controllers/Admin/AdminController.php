<?php
/**
 * Created by Raghu Liu
 * Date: 2022/9/17
 * Time: 21:34
 */

namespace App\Http\Controllers\Admin;

use App\Core\AdminAuth;
use App\Core\BaseController;
use App\Repository\AdminRepository;
use App\Service\AdminRoleService;
use App\Service\LogService;
use Illuminate\Http\Request;

class AdminController extends BaseController
{
    private $adminRepository;
    private $adminRoleService;

    public function __construct(AdminRepository $adminRepository, AdminRoleService $adminRoleService)
    {
        $this->adminRepository  = $adminRepository;
        $this->adminRoleService = $adminRoleService;
    }

    /**
     * @Desc: 获取管理员列表
     * @return array
     * @author: Raghu Liu
     * @Time: 2023/5/12 21:40
     */
    public function browse()
    {
        $list = $this->adminRepository
            ->setByRequest()
            ->getList();
        return $this->success($list);
    }

    public function add(Request $request)
    {
        $this->adminRepository->addAdmin(
            $request->input('username'),
            $request->input('password'),
            $request->input('name'),
            $request->input('avatar_image_id'),
            $request->input('status'),
            $request->input('super')
        );

        LogService::add('新增管理员', 'add');

        return $this->success();
    }

    public function edit(Request $request)
    {
        $this->adminRepository->editAdmin(
            $request->input('admin_id'),
            $request->input('password'),
            $request->input('name'),
            $request->input('avatar_image_id'),
            $request->input('status'),
            $request->input('super')
        );

        $this->adminRoleService->assignRole($request->input('admin_id'), $request->input('admin_role_list'));

        LogService::add('编辑管理员', 'edit');

        return $this->success();
    }

    public function remove(Request $request)
    {
        $this->adminRepository->delete($request->input('admin_id'));

        LogService::add('删除管理员', 'remove');

        return $this->success();
    }
}
