<?php
/**
 * Created by Raghu Liu
 * Date: 2022/9/17
 * Time: 21:34
 */

namespace App\Http\Controllers\Admin;

use App\Core\AdminAuth;
use App\Core\BaseController;
use App\Repository\AdminRouteRepository;
use App\Repository\PermissionRepository;
use App\Service\AdminPermissionService;
use Illuminate\Http\Request;

class AdminRouteController extends BaseController
{
    private $adminRouteRepository;
    private $permissionRepository;

    public function __construct(AdminRouteRepository $adminRouteRepository, PermissionRepository $permissionRepository)
    {
        $this->adminRouteRepository = $adminRouteRepository;
        $this->permissionRepository = $permissionRepository;
    }

    /**
     * @Desc: 获取后台菜单列表
     * @return array
     * @author: Raghu Liu
     * @Time: 2023/9/9 13:22
     */
    public function browse()
    {
        $list = $this->adminRouteRepository
            ->setByRequest()
            ->adminQuery();
        return $this->success($list);
    }

    /**
     * @Desc: 添加菜单
     * @param Request $request
     * @return array
     * @throws \App\Exceptions\ApiException
     * @author: Raghu Liu
     * @Time: 2023/9/9 18:25
     */
    public function add(Request $request)
    {
        $this->adminRouteRepository->allowFieldCreate([
            'name',
            'title',
            'parent_id',
            'path',
            'component',
            'icon',
            'order'
        ], $request->input());
        return $this->success();
    }

    /**
     * @Desc: 编辑菜单
     * @param Request $request
     * @return array
     * @throws \App\Exceptions\ApiException
     * @author: Raghu Liu
     * @Time: 2023/9/9 18:26
     */
    public function edit(Request $request)
    {
        $this->adminRouteRepository->allowFieldUpdate([
            'name',
            'title',
            'parent_id',
            'path',
            'component',
            'icon',
            'order'
        ], $request->input(), $request->input('admin_route_id'));
        return $this->success();
    }

    /**
     * @Desc: 删除菜单
     * @param Request $request
     * @return array
     * @throws \App\Exceptions\ApiException
     * @author: Raghu Liu
     * @Time: 2023/9/17 12:01
     */
    public function remove(Request $request)
    {
        $this->adminRouteRepository->delete($request->input('admin_route_id'));
        return $this->success();
    }

    public function setPermission(Request $request)
    {
        AdminPermissionService::setPermissionList($request->input('control'), $request->input('permission_list'));
        return $this->success();
    }

    /**
     * @Desc: 获取方法列表
     * @param Request $request
     * @return array
     * @author: Raghu Liu
     * @Time: 2023/9/17 14:31
     */
    public function getPermissionList(Request $request)
    {
        $list = $this->permissionRepository->getByControl($request->input('control'));
        return $this->success($list);
    }
}
