<?php
/**
 * Created by Raghu Liu
 * Date: 2022/9/25
 * Time: 16:49
 */

namespace App\Http\Controllers\Admin;

use App\Core\AdminAuth;
use App\Core\BaseController;
use App\Repository\AdminRepository;
use App\Repository\AdminRoleRepository;
use App\Repository\AdminRouteRepository;
use App\Repository\ProvinceRepository;
use App\Service\FileService;
use App\Service\ImageService;
use Illuminate\Http\Request;

class CommonController extends BaseController
{
    private $adminRoleRepository;
    private $provinceRepository;
    private $adminRouteRepository;
    private $adminRepository;

    public function __construct(AdminRoleRepository  $adminRoleRepository,
                                ProvinceRepository   $provinceRepository,
                                AdminRouteRepository $adminRouteRepository,
                                AdminRepository      $adminRepository)
    {
        $this->adminRoleRepository  = $adminRoleRepository;
        $this->provinceRepository   = $provinceRepository;
        $this->adminRouteRepository = $adminRouteRepository;
        $this->adminRepository      = $adminRepository;
    }

    /**
     * @Desc: 上传图片
     * @param Request $request
     * @return array
     * @author: Raghu Liu
     * @Time: 2023/9/17 9:51
     */
    public function uploadImage(Request $request)
    {
        $image = ImageService::saveImage('admin', $request->file('image'), $request->input('access', 'public'));
        return $this->success($image);
    }

    /**
     * @Desc: 获取权限组选项
     * @return array
     * @author: Raghu Liu
     * @Time: 2023/5/13 12:35
     */
    public function getAdminRoleOptions()
    {
        $list = $this->adminRoleRepository->getPairs();
        return $this->success($list);
    }

    /**
     * @Desc: 获取省份选项
     * @return array
     * @author: Raghu Liu
     * @Time: 2023/9/24 10:45
     */
    public function getProvinceOptions()
    {
        $list = $this->provinceRepository->getPairs();
        return $this->success($list);
    }

    /**
     * @Desc: 获取后台用户选项
     * @return array
     * @author: Raghu Liu
     * @Time: 2023/9/24 10:45
     */
    public function getAdminOptions()
    {
        $list = $this->adminRepository->getOptions();
        return $this->success($list);
    }

    /**
     * @Desc: 获取省市区选项
     * @return array
     * @author: Raghu Liu
     * @Time: 2023/9/9 13:49
     */
    public function getLocation()
    {
        $list = $this->provinceRepository->getTree();
        return $this->success($list);
    }

    /**
     * @Desc: 获取后台菜单选项
     * @return array
     * @author: Raghu Liu
     * @Time: 2023/9/9 13:49
     */
    public function getAdminRouteOptions()
    {
        $list = $this->adminRouteRepository->getOptions();

        return $this->success($list);
    }

    /**
     * @Desc: 上传文件
     * @param Request $request
     * @return array
     * @author: Raghu Liu
     * @Time: 2023/9/17 9:51
     */
    public function uploadFile(Request $request)
    {
        $image = FileService::saveFile('admin', $request->file('file'), $request->input('access', 'public'));
        return $this->success($image);
    }
}
