<?php

namespace App\Http\Controllers\Admin;

use App\Core\AdminAuth;
use App\Core\BaseController;
use App\Repository\TodoRepository;
use App\Service\AdminNoticeService;
use Illuminate\Http\Request;

class TodoController extends BaseController
{

    public function __construct(TodoRepository $todoRepository)
    {
        $this->todoRepository = $todoRepository;
    }

    /**
     * @Desc: 浏览待办
     * @return array
     * @author: Raghu Liu
     * @Time: 2023/3/14 20:57
     */
    public function browse()
    {
        $adminID = AdminAuth::id();

        $list = $this->todoRepository->setByRequest()->getMyList($adminID);

        return $this->success($list);
    }

    /**
     * @Desc: 新增待办
     * @param Request $request
     * @return array
     * @throws \App\Exceptions\ApiException
     * @author: Raghu Liu
     * @Time: 2023/3/14 20:57
     */
    public function add(Request $request)
    {
        $data = [
            'title'             => $request->input('title'),
            'multi_day'         => $request->input('multi_day'),
            'all_day'           => $request->input('all_day') ? 1 : 0,
            'assignto_admin_id' => $request->input('assignto_admin_id'),
            'admin_id'          => AdminAuth::id(),
            'desc'              => $request->input('desc')
        ];
        if ($data['multi_day']) {
            $data['start'] = $request->input('date_range.0');
            $data['end']   = $request->input('date_range.1');
        } else {
            $data['start'] = $request->input('start') . ' ' . $request->input('start_time');
            $data['end']   = $request->input('start') . ' ' . $request->input('end_time');
        }

        $this->todoRepository->create($data);

        if ($data['admin_id'] != $data['assignto_admin_id']) {
            AdminNoticeService::send($data['admin_id'], $data['assignto_admin_id'], '待办提醒', $data['title']);
        }

        return $this->success();
    }

    /**
     * @Desc: 编辑待办
     * @param Request $request
     * @return array
     * @throws \App\Exceptions\ApiException
     * @author: Raghu Liu
     * @Time: 2023/3/14 20:57
     */
    public function edit(Request $request)
    {
        $todoID = $request->input('todo_id');
        $data   = [
            'title'             => $request->input('title'),
            'multi_day'         => $request->input('multi_day'),
            'assignto_admin_id' => $request->input('assignto_admin_id'),
            'all_day'           => $request->input('all_day') ? 1 : 0,
            'desc'              => $request->input('desc')
        ];
        if ($data['multi_day']) {
            $data['start'] = $request->input('date_range.0');
            $data['end']   = $request->input('date_range.1');
        } else {
            $data['start'] = $request->input('start') . ' ' . $request->input('start_time');
            $data['end']   = $request->input('start') . ' ' . $request->input('end_time');
        }
        $this->todoRepository->update($data, $todoID);

        return $this->success();
    }

    /**
     * @Desc: 删除待办
     * @param Request $request
     * @return array
     * @throws \Exception
     * @author: Raghu Liu
     * @Time: 2023/3/15 20:39
     */
    public function remove(Request $request)
    {
        $adminID = AdminAuth::id();
        $todoId  = $request->input('todoId');

        $this->todoRepository->deleteWhere(['admin_id' => $adminID, 'todo_id' => $todoId]);

        return $this->success();
    }
}
