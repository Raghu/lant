<?php
namespace App\Http\Controllers\Admin;

use App\Core\BaseController;
use App\Repository\ConfigRepository;
use App\Service\LogService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ConfigController extends BaseController
{
    private $configRepository;

    public function __construct(ConfigRepository $configRepository)
    {
        $this->configRepository = $configRepository;
    }

    /**
     * @Desc: 浏览配置
     * @return array
     * @author: Raghu Liu
     * @Time: 2023/9/19 22:14
     */
    public function browse()
    {
        $list = $this->configRepository->adminQuery();
        return $this->success($list);
    }

    public function edit(Request $request)
    {
        DB::beginTransaction();
        try {
            $this->configRepository->save($request->all());

            LogService::add('编辑配置项', 'edit');

            DB::commit();
            return $this->success();
        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
    }
}
