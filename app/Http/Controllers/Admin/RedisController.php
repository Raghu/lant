<?php
/**
 * Created by Raghu Liu
 * Date: 2023/9/23
 * Time: 10:53
 */

namespace App\Http\Controllers\Admin;

use App\Core\BaseController;
use App\Core\RedisCache;
use Illuminate\Http\Request;

class RedisController extends BaseController
{
    /**
     * @Desc: 获取redis key列表
     * @return mixed
     */
    public function getKeyList()
    {
        $redisKey     = config('rediskey');
        $redisKeyList = [];
        foreach ($redisKey as $key => $value) {
            $redisKeyList[] = [
                'key'  => $key,
                'name' => $value::$name,
                'desc' => $value::$desc,
                'type' => $value::$type,
            ];
        }
        return $this->success($redisKeyList);
    }

    /**
     * @Desc: 获取redis key列表
     * @param Request $request
     * @return mixed
     */
    public function browse(Request $request)
    {
        $key      = $request->input('key');
        $redisKey = config('rediskey');
        if (!isset($redisKey[$key])) {
            return $this->error('key不存在');
        }
        $redisKey = $redisKey[$key];
        $list     = $redisKey::allKeys();
        return $this->success($list);
    }

    /**
     * @Desc: 获取值
     * @param Request $request
     * @return array
     * @author: Raghu Liu
     * @Time: 2023/9/23 14:57
     */
    public function info(Request $request)
    {
        $key  = $request->input('key');
        $info = RedisCache::getInfo($key);
        return $this->success($info);
    }

    /**
     * @Desc: 编辑值
     * @param Request $request
     * @return array
     * @author: Raghu Liu
     * @Time: 2023/9/23 14:57
     */
    public function edit(Request $request)
    {
        $key = $request->input('key');
        RedisCache::set($key, $request->input('value'));
        return $this->success();
    }

    /**
     * @Desc: 删除键
     * @param Request $request
     * @return array
     * @author: Raghu Liu
     * @Time: 2023/9/23 14:57
     */
    public function remove(Request $request)
    {
        $key = $request->input('key');
        RedisCache::del($key);
        return $this->success();
    }

    /**
     * 设置过期时间
     * @param Request $request
     * @return mixed
     */
    public function ttl(Request $request)
    {
        $key = $request->input('key');
        $ttl = $request->input('ttl');
        RedisCache::EXPIRE($key, $ttl);
        return $this->success();
    }
}
