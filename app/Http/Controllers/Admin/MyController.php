<?php

namespace App\Http\Controllers\Admin;

use App\Core\AdminAuth;
use App\Core\BaseController;
use App\Repository\AdminNoticeRepository;
use App\Repository\AdminRepository;
use Illuminate\Http\Request;

class MyController extends BaseController
{
    private $adminRepository;
    private $adminNoticeRepository;

    public function __construct(AdminRepository $adminRepository, AdminNoticeRepository $adminNoticeRepository)
    {
        $this->adminRepository       = $adminRepository;
        $this->adminNoticeRepository = $adminNoticeRepository;
    }

    /**
     * @Desc: 修改个人信息
     * @param Request $request
     * @return array
     * @throws \App\Exceptions\ApiException
     * @author: Raghu Liu
     * @Time: 2023/9/24 12:31
     */
    public function setInfo(Request $request)
    {
        $adminId = AdminAuth::id();

        $password      = $request->input('password');
        $avatarImageId = $request->input('avatar_image_id');
        $this->adminRepository->editMyInfo($adminId, $password, $avatarImageId);

        return $this->success();
    }

    /**
     * @Desc: 获取消息通知
     * @author: Raghu Liu
     * @Time: 2023/9/24 12:32
     */
    public function getNoticeList()
    {
        $list = $this->adminNoticeRepository->getMy();

        return $this->success($list);
    }

    /**
     * @Desc: 标记消息已读
     * @param Request $request
     * @return array
     * @throws \App\Exceptions\ApiException
     * @author: Raghu Liu
     * @Time: 2023/9/24 13:12
     */
    public function readNotice(Request $request)
    {
        $this->adminNoticeRepository->updateWhere(['is_read' => 1], ['admin_notice_id' => $request->input('admin_notice_id'), 'admin_id' => AdminAuth::id()]);

        return $this->success();
    }
}
