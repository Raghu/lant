<?php

namespace App\Http\Controllers\Admin;

use App\Core\AdminAuth;
use App\Core\BaseController;
use App\Repository\ActivelyLogRepository;
use App\Repository\LogRepository;
use Illuminate\Http\Request;

class ActivelyLogController extends BaseController
{
    private $activelyLogRepository;

    public function __construct(ActivelyLogRepository $activelyLogRepository)
    {
        $this->activelyLogRepository = $activelyLogRepository;
    }

    /**
     * @Desc: 浏览日志
     * @param Request $request
     * @return array
     * @author: Raghu Liu
     * @Time: 2023/3/16 21:23
     */
    public function browse()
    {
        $list = $this->activelyLogRepository->setByRequest()->adminQuery();

        return $this->success($list);
    }
}
