<?php

namespace App\Http\Controllers\Admin;

use App\Core\BaseController;
use App\Repository\AdminLoginLogRepository;
use App\Repository\AdminOperationLogRepository;

class LogController extends BaseController
{
    private $adminOperationLogRepository;
    private $adminLoginLogRepository;

    public function __construct(AdminOperationLogRepository $adminOperationLogRepository,
                                AdminLoginLogRepository     $adminLoginLogRepository)
    {
        $this->adminOperationLogRepository = $adminOperationLogRepository;
        $this->adminLoginLogRepository = $adminLoginLogRepository;
    }

    public function adminLogin()
    {
        $list = $this->adminLoginLogRepository
            ->setByRequest()
            ->adminQuery();
        return $this->success($list);
    }

    public function adminOperation()
    {
        $list = $this->adminOperationLogRepository
            ->setByRequest()
            ->adminQuery();
        return $this->success($list);
    }

}
