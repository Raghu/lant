<?php
/**
 * Created by Raghu Liu
 * Date: 2022/9/17
 * Time: 21:34
 */

namespace App\Http\Controllers\Admin;

use App\Core\BaseController;
use App\Repository\AdminRoleRepository;
use App\Service\AdminPermissionService;
use App\Service\AdminRoleService;
use Illuminate\Http\Request;

class AdminRoleController extends BaseController
{
    private $adminRoleRepository;
    private $adminRoleService;

    public function __construct(AdminRoleRepository $adminRoleRepository, AdminRoleService $adminRoleService)
    {
        $this->adminRoleRepository = $adminRoleRepository;
        $this->adminRoleService    = $adminRoleService;
    }

    /**
     * @Desc: 浏览角色
     * @param Request $request
     * @return array
     * @author: Raghu Liu
     * @Time: 2023/5/13 12:06
     */
    public function browse(Request $request)
    {
        $list = $this->adminRoleRepository
            ->setByRequest()
            ->getList();

        return $this->success($list);
    }

    /**
     * @Desc: 添加角色
     * @param Request $request
     * @return array
     * @throws \App\Exceptions\ApiException
     * @author: Raghu Liu
     * @Time: 2023/5/13 12:06
     */
    public function add(Request $request)
    {
        try {
            $data = $request->input();
            $this->adminRoleRepository->create($data);
            return $this->success();
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @Desc: 编辑角色
     * @param Request $request
     * @return array
     * @throws \App\Exceptions\ApiException
     * @author: Raghu Liu
     * @Time: 2023/5/13 12:06
     */
    public function edit(Request $request)
    {
        try {
            $data = $request->input();
            $this->adminRoleRepository->update($data, $request->input('id'));
            return $this->success();
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @Desc: 删除角色
     * @param Request $request
     * @return array
     * @throws \App\Exceptions\ApiException
     * @author: Raghu Liu
     * @Time: 2023/5/13 12:06
     */
    public function remove(Request $request)
    {
        $this->adminRoleRepository->delete($request->input('id'));
        return $this->success();
    }

    /**
     * @Desc: 获取某角色拥有权限
     * @param Request $request
     * @return array
     * @author: Raghu Liu
     * @Time: 2023/5/13 12:06
     */
    public function getPermission(Request $request)
    {
        $adminRoleId = $request->input('id');
        $data         = $this->adminRoleService->getPermissionGroup($adminRoleId);

        return $this->success(array('data' =>$data));
    }

    public function setPermission(Request $request)
    {
        $adminGroupId = $request->input('adminGroupId');
        $data         = $request->input('data');

        $this->adminRoleService->setPermissionList($adminGroupId, $data);

        return $this->success();
    }

}
