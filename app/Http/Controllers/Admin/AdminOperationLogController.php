<?php
/**
 * Created by Raghu Liu
 * Date: 2023/9/23
 * Time: 20:10
 */

namespace App\Http\Controllers\Admin;

use App\Core\BaseController;
use App\Repository\AdminOperationLogRepository;
use Illuminate\Http\Request;

class AdminOperationLogController extends BaseController
{
    protected $adminOperationLogRepository;

    public function __construct(AdminOperationLogRepository $adminOperationLogRepository)
    {
        $this->adminOperationLogRepository = $adminOperationLogRepository;
    }

    public function browse(Request $request)
    {
        $list = $this->adminOperationLogRepository->setByRequest()->adminQuery();

        return $this->success($list);
    }
}
