<?php

namespace App\Http\Controllers\Admin;

use App\Core\AdminAuth;
use App\Core\BaseController;
use App\Service\AdminAuthService;
use App\Service\AdminRoleService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

class IndexController extends BaseController
{
    private $adminAuthService;
    private $adminRoleService;

    public function __construct(AdminAuthService $adminAuthService, AdminRoleService $adminRoleService)
    {
        $this->adminRoleService = $adminRoleService;
        $this->adminAuthService = $adminAuthService;
    }

    /**
     * @Desc: 后台登录
     * @param Request $request
     * @return array
     * @author: Raghu Liu
     * @Time: 2022/9/17 13:49
     */
    public function login(Request $request)
    {
        $ip         = $request->getClientIp();
        $user_agent = $request->server('HTTP_USER_AGENT');
        $admin      = $this->adminAuthService->login($request->input('username'), $request->input('password'), $ip, $user_agent);
        return $this->success([
            'token' => $admin->token,
            'name'  => $admin->name
        ]);
    }

    /**
     * @Desc: 获取个人信息
     * @param Request $request
     * @return array
     * @author: Raghu Liu
     * @Time: 2023/5/14 16:27
     */
    public function getUserInfo(Request $request)
    {
        $admin = auth()->user();

        list($menu, $actions) = $this->adminRoleService->getMenuList($admin);

        return $this->success([
            'info'    => [
                'admin_id'        => $admin->admin_id,
                'username'        => $admin->username,
                'name'            => $admin->name,
                'avatar'          => $admin->avatar,
                'avatar_image_id' => $admin->avatar_image_id,
            ],
            'menu'    => $menu,
            'actions' => $actions
        ]);
    }

    /**
     * @Desc: 绑定socket id
     * @param Request $request
     * @return array
     * @throws \App\Exceptions\BusinessException
     * @throws \App\Exceptions\ParameterException
     * @throws \App\Exceptions\TokenException
     * @throws \Throwable
     * @Time: 2023/5/14 16:27
     */
    public function bindSocketId(Request $request)
    {
        $admin_id  = AdminAuth::id();
        $socket_id = $request->input('socketId');
        $socket_id = Crypt::decryptString($socket_id);
        $this->adminAuthService->bindSocketId($admin_id, $socket_id);
        return $this->success();
    }
}
