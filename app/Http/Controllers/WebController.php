<?php
/**
 * Created by Raghu Liu
 * Date: 2022/10/5
 * Time: 15:16
 */

namespace App\Http\Controllers;

use App\Core\RedisCache;
use App\Models\File;
use App\Models\Image;
use Illuminate\Http\Request;

class WebController extends Controller
{
    /**
     * @Desc: 下载图片
     * @param Request $request
     * @return never|\Symfony\Component\HttpFoundation\BinaryFileResponse
     * @author: Raghu Liu
     * @Time: 2023/9/17 11:09
     */
    public function image(Request $request)
    {
        $code = $request->route('code');
        $key  = $request->get('key');
        $file = Image::where('code', $code)->first();

        if ($file->access !== 'public') {
            $cache_id = RedisCache::get($key);

            if (empty($cache_id) || $cache_id != $code) return abort(404);
        }

        return response()->file(storage_path() . '/app/' . $file->path);
    }

    /**
     * @Desc: 下载文件
     * @param Request $request
     * @return never|\Symfony\Component\HttpFoundation\BinaryFileResponse
     * @author: Raghu Liu
     * @Time: 2023/9/17 11:09
     */
    public function file(Request $request)
    {
        $code = $request->route('code');
        $key  = $request->get('key');
        $file = File::where('code', $code)->first();

        if ($file->access !== 'public') {
            $cache_id = RedisCache::get($key);

            if (empty($cache_id) || $cache_id != $code) return abort(404);
        }

        return response()->file(storage_path() . '/app/' . $file->path);
    }
}
