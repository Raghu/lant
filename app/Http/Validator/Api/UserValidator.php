<?php
/**
 * Created by Raghu Liu
 * Date: 2022/9/17
 * Time: 12:51
 */

namespace App\Http\Validator\Api;

use Illuminate\Validation\Rule;

class  UserValidator
{
    public static function hellow()
    {
        return [
            'rule'    => [
                'test' => 'required',
            ],
            'message' => [
                'test.required' => '需要传一个test参数'
            ]
        ];
    }
}
