<?php
/**
 * Created by Raghu Liu
 * Date: 2022/9/25
 * Time: 17:36
 */

namespace App\Http\Validator\Admin;

class CommonValidator
{
    public static function uploadImage()
    {
        return [
            'rule'    => [
                'image' => 'required|file'
            ],
            'message' => [
                'image.required' => '请选择图片',
            ]
        ];
    }

    public static function uploadFile()
    {
        return [
            'rule'    => [
                'file' => 'required|file'
            ],
            'message' => [
                'image.required' => '请选择文件',
            ]
        ];
    }
}
