<?php
/**
 * Created by Raghu Liu
 * Date: 2022/9/17
 * Time: 12:51
 */

namespace App\Http\Validator\Admin;

class AdminRouteValidator
{
    public static function add()
    {
        return [
            'rule'    => [
                'name'      => 'required|regex:/^[A-Za-z]+$/',
                'title'     => 'required',
                'path'      => 'required',
                'component' => 'required'
            ],
            'message' => [
                'name.required'      => '组件名不能为空',
                'name.regex'         => '组件名只能使用英文字母',
                'title.required'     => '菜单标题不能为空',
                'path.required'      => '访问地址不能为空',
                'component.required' => '组件路径不能为空'
            ]
        ];
    }

    public static function edit()
    {
        return [
            'rule'    => [
                'admin_route_id' => 'required',
                'name'           => 'required|regex:/^[A-Za-z]+$/',
                'title'          => 'required',
                'path'           => 'required',
                'component'      => 'required'
            ],
            'message' => [
                'name.required'      => '组件名不能为空',
                'name.regex'         => '组件名只能使用英文字母',
                'title.required'     => '菜单标题不能为空',
                'path.required'      => '访问地址不能为空',
                'component.required' => '组件路径不能为空'
            ]
        ];
    }

    public static function remove()
    {
        return [
            'rule'    => [
                'admin_route_id' => 'required'
            ]
        ];
    }
}
