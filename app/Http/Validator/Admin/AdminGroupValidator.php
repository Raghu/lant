<?php
/**
 * Created by Raghu Liu
 * Date: 2022/9/17
 * Time: 12:51
 */

namespace App\Http\Validator\Admin;

class AdminGroupValidator
{
    public static function login()
    {
        return [
            'rule' => [
                'username' => 'required',
                'password' => 'required'
            ],
            'message' => [
                'username.required' => '请输入用户名',
                'password.required' => '请输入密码'
            ]
        ];
    }
}
