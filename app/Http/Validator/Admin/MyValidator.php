<?php
/**
 * Created by Raghu Liu
 * Date: 2022/9/17
 * Time: 12:51
 */

namespace App\Http\Validator\Admin;

class MyValidator
{
    public static function setInfo()
    {
        return [
            'rule'    => [
                'password'        => 'confirmed',
                'avatar_image_id' => 'required'
            ],
            'message' => [
                'password.confirmed'       => '两次密码输入不一致',
                'avatar_image_id.required' => '请上传头像'
            ]
        ];
    }
}
