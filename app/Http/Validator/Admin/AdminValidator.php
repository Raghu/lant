<?php
/**
 * Created by Raghu Liu
 * Date: 2022/9/17
 * Time: 12:51
 */

namespace App\Http\Validator\Admin;

class AdminValidator
{
    public static function add()
    {
        return [
            'rule' => [
                'username' => 'required|unique:admin,username|alpha_num|between:4,20',
                'name' => 'required',
                'password' => 'required|confirmed',
                'password_confirmation' => 'required',
                'admin_role_list' => 'required',
                'avatar_image_id' => 'required',
                'status' => 'required'
            ],
            'message' => [
                'username.required' => '用户名不能为空',
                'username.unique' => '用户名已存在',
                'username.alpha_num' => '用户名只能是英文或数字',
                'username.between' => '用户名长度应在4-20之间',
                'name.required' => '昵称不能为空',
                'password.required' => '密码不能为空',
                'password.confirmed' => '两次密码输入不一致',
                'password_confirmation.required' => '请重输密码',
                'avatar_image_id.required' => '请上传头像',
                'admin_role_list.required' => '权限组不能为空'
            ]
        ];
    }
    public static function edit()
    {
        return [
            'rule' => [
                'name' => 'required',
                'password' => 'confirmed',
                'admin_role_list' => 'required',
                'avatar_image_id' => 'required',
                'status' => 'required'
            ],
            'message' => [
                'name.required' => '昵称不能为空',
                'password.confirmed' => '两次密码输入不一致',
                'avatar_image_id.required' => '请上传头像',
                'admin_role_list.required' => '权限组不能为空'
            ]
        ];
    }
}
