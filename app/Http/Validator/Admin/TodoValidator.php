<?php
/**
 * Created by Raghu Liu
 * Date: 2022/9/17
 * Time: 12:51
 */

namespace App\Http\Validator\Admin;

use Illuminate\Validation\Rule;

class  TodoValidator
{
    public static function add()
    {
        $request = request();

        return [
            'rule'    => [
                'title'      => 'required',
                'start'      => 'required_if:multi_day,0',
                'date_range' => 'required_if:multi_day,1',
                'start_time' => Rule::requiredIf(function () use ($request) {
                    return !$request->all_day;
                }),
                'end_time'   => Rule::requiredIf(function () use ($request) {
                    return !$request->all_day;
                })
            ],
            'message' => [
                'title.required'         => '请输入标题',
                'start.required_if'      => '请选择日期',
                'date_range.required_if' => '请选择起止时间',
                'start_time.required'    => '请选择开始时间',
                'end_time.required'      => '请选择结束时间'
            ]
        ];
    }

    public static function edit()
    {
        $request = request();

        return [
            'rule'    => [
                'title'      => 'required',
                'start'      => 'required_if:multi_day,0',
                'date_range' => 'required_if:multi_day,1',
                'start_time' => Rule::requiredIf(function () use ($request) {
                    return !$request->all_day;
                }),
                'end_time'   => Rule::requiredIf(function () use ($request) {
                    return !$request->all_day;
                })
            ],
            'message' => [
                'title.required'         => '请输入标题',
                'start.required_if'      => '请选择日期',
                'date_range.required_if' => '请选择起止时间',
                'start_time.required'    => '请选择开始时间',
                'end_time.required'      => '请选择结束时间'
            ]
        ];
    }
}
