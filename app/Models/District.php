<?php

namespace App\Models;

use App\Core\BaseModel;
use Illuminate\Database\Eloquent\Builder;

class District extends BaseModel
{
    protected $table = 'district';
    protected $primaryKey = 'district_id';

    public function scopeSimpleScene(Builder $builder)
    {
        return $builder->select([
            'province_id',
            'name',
            'short_name',
            'short_name as label',
            'district_id',
            'district_id as value',
            'city_id'
        ]);
    }
}
