<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Core\BaseModel;

class AdminLoginLog extends BaseModel
{
    protected $table = 'admin_login_log';
    protected $primaryKey = 'admin_login_log_id';
}
