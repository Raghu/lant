<?php
/**
 * Created by Raghu Liu
 * Date: 2023/9/17
 * Time: 9:53
 */

namespace App\Models;

use App\Core\BaseModel;
use App\Core\RedisCache;

class File extends BaseModel
{
    protected $table = 'file';

    protected $primaryKey = 'file_id';

    protected $appends = ['url'];

    protected $hidden = ['filename'];

    public function getUrlAttribute()
    {
        if (!isset($this->attributes['code'])) return '';

        $code = $this->attributes['code'];

        $access  = $this->attributes['access'];
        if ($code) {
            if ($access == 'public')
            {
                return request()->root() . '/file/' . $code;
            }
            else
            {
                $key = md5(time() . $code);
                RedisCache::set($key, $code, 86400);
                return request()->root() . '/file/' . $code . '?key=' . $key;
            }
        }
    }
}
