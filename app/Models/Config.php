<?php

namespace App\Models;

use App\Core\BaseModel;
use Spatie\Activitylog\Traits\LogsActivity;

class Config extends BaseModel
{
    use  LogsActivity;

    protected $table = 'config';
    protected $primaryKey = 'config_id';
    public $timestamps = false;

    protected static $logAttributes = ['name', 'value'];
    protected static $logName = 'config';
    protected static $logOnlyDirty = true;

    public function image()
    {
        return $this->belongsTo(Image::class, 'value', 'image_id')->withDefault([
            'url' => ''
        ]);
    }
    public function video()
    {
        return $this->belongsTo(File::class, 'value', 'file_id')->withDefault([
            'url' => ''
        ]);
    }
}
