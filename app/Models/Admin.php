<?php

namespace App\Models;

use App\Core\BaseModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Traits\HasPermissions;
use Spatie\Activitylog\Traits\LogsActivity;

class Admin extends BaseModel
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles, HasFactory, HasPermissions, LogsActivity;

    protected static $logAttributes = ['username', 'name', 'avatar_image_id', 'admin_role_id', 'status'];
    protected static $logName = 'admin';
    protected static $logOnlyDirty = true;

    public $table = 'admin';

    public $primaryKey = 'admin_id';

    protected $hidden = ['password'];

    public function avatar()
    {
        return $this->hasOne(Image::class, 'image_id', 'avatar_image_id')->withDefault();
    }

    public function scopeMasking($query)
    {
        return $query->select(['admin_id', 'username', 'name', 'avatar_image_id'])->with(['avatar']);
    }
}
