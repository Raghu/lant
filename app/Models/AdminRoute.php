<?php
/**
 * Created by Raghu Liu
 * Date: 2022/9/17
 * Time: 16:45
 */

namespace App\Models;

use App\Core\BaseModel;

class AdminRoute extends BaseModel
{
    protected $table = 'admin_route';
    protected $primaryKey = 'admin_route_id';

    protected static function boot()
    {
        parent::boot();

        static::updating(function ($adminRoute) {
            if ($adminRoute->isDirty('name')) {
                Permission::where('control', $adminRoute->getOriginal('name'))->update(['control' => $adminRoute->name]);
            }
        });

        static::deleting(function ($adminRoute) {
            AdminRoute::where('parent_id', $adminRoute->admin_route_id)->delete();
            Permission::where('control', $adminRoute->getOriginal('name'))->delete();
        });
    }

    public function children()
    {
        return $this->hasMany(AdminRoute::class, 'parent_id', 'admin_route_id');
    }

    public function permission()
    {
        return $this->hasMany(Permission::class, 'control', 'name');
    }
}
