<?php

namespace App\Models;

use App\Core\BaseModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Activitylog\Traits\LogsActivity;

class User extends BaseModel
{
    use HasApiTokens, HasFactory, Notifiable, HasFactory, LogsActivity;

    protected static $logAttributes = ['username', 'name', 'avatar_image_id', 'admin_role_id', 'status'];
    protected static $logName = 'user';
    protected static $logOnlyDirty = true;

    public $table = 'user';

    public $primaryKey = 'user_id';
}
