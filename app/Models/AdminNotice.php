<?php
/**
 * Created by Raghu Liu
 * Date: 2023/9/24
 * Time: 12:15
 */
namespace App\Models;

use App\Core\BaseModel;

class AdminNotice extends BaseModel
{
    protected $table = 'admin_notice';
    protected $primaryKey = 'admin_notice_id';

    public function sendAdmin()
    {
        return $this->belongsTo(Admin::class, 'send_admin_id', 'admin_id');
    }
}
