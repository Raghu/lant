<?php

namespace App\Models;

use App\Core\BaseModel;

class Todo extends BaseModel
{
    public $table = 'todo';

    public $primaryKey = 'todo_id';

    protected $appends = ['name'];

    public function getNameAttribute()
    {
        return $this->title;
    }

    public function logs()
    {
        return $this->morphMany(Log::class,'object','Type','ID');
    }
}
