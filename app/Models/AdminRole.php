<?php

namespace App\Models;

use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

class AdminRole extends Role
{
    const UPDATED_AT = null;
    public $guarded = [];

    public function setGuardNameAttribute()
    {
        $this->attributes['guard_name'] = 'admin';
    }
}
