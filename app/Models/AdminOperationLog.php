<?php

namespace App\Models;

use App\Core\BaseModel;

class AdminOperationLog extends BaseModel
{
    protected $table = 'admin_operation_log';
    protected $primaryKey = 'admin_operation_log_id';

    public function admin()
    {
        return $this->belongsTo(Admin::class, 'admin_id', 'admin_id');
    }

    public function activityLog()
    {
        return $this->hasMany(ActivelyLog::class, 'admin_operation_log_id', 'admin_operation_log_id');
    }
}
