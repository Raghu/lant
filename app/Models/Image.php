<?php

namespace App\Models;

use App\Core\BaseModel;
use App\Core\RedisCache;

class Image extends BaseModel
{
    public $table = 'image';

    public $primaryKey = 'image_id';

    protected $appends = ['url'];

    protected $hidden = ['filename'];

    public function getUrlAttribute()
    {
        if (!isset($this->attributes['code'])) return '';

        $code = $this->attributes['code'];

        $access  = $this->attributes['access'];
        if ($code) {
            if ($access == 'public')
            {
                return request()->root() . '/image/' . $code;
            }
            else
            {
                $key = md5(time() . $code);
                RedisCache::set($key, $code, 86400);
                return request()->root() . '/image/' . $code . '?key=' . $key;
            }
        }
    }
}
