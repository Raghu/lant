<?php

namespace App\Models;

use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission as SpatiePermission;

class Permission extends SpatiePermission
{
    protected $guard_name = 'admin';

    public $guarded = [];
    public $timestamps = false;

    protected $fillable = ['control'];
}
