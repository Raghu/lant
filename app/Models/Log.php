<?php

namespace App\Models;

use App\Core\AdminActionName;
use App\Core\BaseModel;

class Log extends BaseModel
{
    public $table = 'log';

    public $primaryKey = 'logID';

    protected $appends = ['objectName', 'actionName'];

    protected $objectMap = [
        'todo' => '待办'
    ];

    public function getActionNameAttribute($key)
    {
        $key = $this->objectType . '_' . $this->action;
        $data = AdminActionName::getData();
        return isset($data[$key]) ? $data[$key] : $this->action;
    }

    public function getObjectNameAttribute($key)
    {
        return $this->objectMap[$this->objectType];
    }

    public function admin()
    {
        return $this->hasOne(Admin::class, 'adminID', 'adminID')->masking();
    }

    public function object()
    {
        return $this->morphTo('object', 'objectType', 'objectID');
    }
}
