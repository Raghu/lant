<?php

namespace App\Models;

use App\Core\BaseModel;
use Illuminate\Database\Eloquent\Builder;

class Province extends BaseModel
{
    protected $table = 'city';
    protected $primaryKey = 'city_id';

    public function scopeSimpleScene(Builder $builder)
    {
        return $builder->select([
            'province_id',
            'city_id as value',
            'name',
            'short_name as label',
            'short_name',
            'city_id'
        ]);
    }

    public function children()
    {
        return $this->hasMany(District::class, 'city_id', 'city_id');
    }
}
