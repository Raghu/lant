<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class table extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:table';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $dbName = env('DB_DATABASE');

        if($this->confirm('该操作将覆盖数据库[' . $dbName . ']中的所有数据，是否继续？',false)){
            $bar = $this->output->createProgressBar(100);
            $bar->start();
            try {
                Artisan::call('migrate');
                Artisan::call('db:seed');

            }catch (\Exception $exception){
                echo "\n";
                $this->error($exception->getMessage());
                return false;
            }
            $bar->finish();

            echo "\n";

            $this->info('项目初始化完成');
            $this->info('初始账号：admin');
            $this->info('初始密码：'. env('DB_PASSWORD'));
            $this->comment('请及时修改密码');
        }
    }
}
