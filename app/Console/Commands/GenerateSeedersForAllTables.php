<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class GenerateSeedersForAllTables extends Command
{
    protected $signature = 'generate:seeders';

    protected $description = '生成所有数据表的Seeder文件';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $tables = \DB::select('SHOW TABLES');
        $databaseName = config('database.connections.mysql.database');
        $key = 'Tables_in_' . $databaseName;

        foreach ($tables as $table) {
            $tableName = $table->$key;
            if ($tableName == 'migrations') {
                continue;
            }

            $className = Str::studly($tableName);
            $seederName = "{$className}TableSeeder";

            $this->call('iseed', [
                'tables' => $tableName,
                '--force' => true
            ]);

            $this->info("Generated seeder for table: $tableName");
        }
    }
}
