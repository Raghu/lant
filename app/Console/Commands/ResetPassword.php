<?php

namespace App\Console\Commands;

use App\Core\PasswordEncrypt;
use App\Models\Admin;
use Illuminate\Console\Command;

class ResetPassword extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:reset-password';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '修改超级管理员的密码。';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $password = $this->secret('请输入新密码:');

        if (empty($password)) {
            $this->error('密码不能为空。');
            return;
        }

        $confirmPassword = $this->secret('请确认密码:');

        if ($password !== $confirmPassword) {
            $this->error('两次输入密码不一致');
            return;
        }

        $admin = Admin::find(1);

        $admin->password = PasswordEncrypt::encrypt($password);
        $admin->save();

        $this->info('密码修改成功。','success');
    }
}
