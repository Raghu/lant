<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class database extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:database';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $dbName   = $this->ask('请输入数据库名称');

        $query = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME =  ?";

        try {
            $db = DB::select($query, [$dbName]);
        } catch (\Exception $e) {
            $this->error('数据库连接失败，请检查：' . $e->getMessage());
            return false;
        }

        if ($db) {
            if($this->confirm('危险：该数据库已存在，请确认是否覆盖，若覆盖将删除该数据库所有数据',false)){
                $this->modifyEnv([
                    'DB_DATABASE' => $dbName
                ]);
            } else {
                $this->handle();
            }
        }

        return 0;
    }

    public function modifyEnv(array $data)
    {
        $envPath = base_path() . DIRECTORY_SEPARATOR . '.env';

        $contentArray = collect(file($envPath, FILE_IGNORE_NEW_LINES));

        $contentArray->transform(function ($item) use ($data) {
            foreach ($data as $key => $value) {
                if (str_contains($item, $key)) {
                    return $key . '=' . $value;
                }
            }

            return $item;
        });

        $content = implode("\n", $contentArray->toArray());

        \File::put($envPath, $content);
    }
}
