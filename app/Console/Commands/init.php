<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class init extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Init project';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $dbUsername = $this->ask('请输入数据库用户名','root');
        $dbPassword = $this->ask('请输入数据库密码');
        $dbName     = $this->ask('请输入数据库名称，该操作将删除该数据库旧数据，请确保该数据库为全新数据库。');
        $dbPrefix   = $this->ask('请输入数据表前缀','db_');

        $this->modifyEnv([
            'DB_USERNAME' => $dbUsername,
            'DB_PASSWORD' => $dbPassword,
            'DB_DATABASE' => $dbName,
            'DB_PREFIX'   => $dbPrefix,
        ]);

        $this->info('设置成功');
	    $this->info('请执行 php artisan command:table 填充数据库数据');

        return 0;
    }

    public function modifyEnv(array $data)
    {
        $envPath = base_path() . DIRECTORY_SEPARATOR . '.env';

        $contentArray = collect(file($envPath, FILE_IGNORE_NEW_LINES));

        $contentArray->transform(function ($item) use ($data) {
            foreach ($data as $key => $value) {
                if (str_contains($item, $key)) {
                    return $key . '=' . $value;
                }
            }

            return $item;
        });

        $content = implode("\n", $contentArray->toArray());

        \File::put($envPath, $content);
    }
}
