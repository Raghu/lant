<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ClearPermissionCache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clear:permission-cache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '清除权限缓存。';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        app(\Spatie\Permission\PermissionRegistrar::class)->forgetCachedPermissions();

        $this->info('权限缓存清理成功');

        return 0;
    }
}
