<?php

namespace App\Providers;

use App\Core\Pagination;
use App\Models\AdminAction;
use App\Models\Todo;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(255);

        // 分页服务注册
        app()->bind('Illuminate\Pagination\LengthAwarePaginator', function ($app, $options) {
            return (new Pagination($options['items'], $options['total'], $options['perPage'], $options['currentPage'], $options['options']));
        });

        $this->bootEloquentMorphs();
    }


    /**
     * 自定义多态关联的类型字段
     */
    private function bootEloquentMorphs()
    {
        Relation::morphMap([
            'todo' => Todo::class
        ]);
    }
}
