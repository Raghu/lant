<?php
/**
 * Created by Raghu Liu
 * Date: 2023/9/24
 * Time: 13:18
 */

namespace App\WebSocket;

use Illuminate\Support\Facades\Crypt;
use Swoole\Http\Request;
use Swoole\WebSocket\Frame;
use Swoole\WebSocket\Server;
use Hhxsv5\LaravelS\Swoole\WebSocketHandlerInterface;

class WebSocketHandler implements WebSocketHandlerInterface
{

    public function __construct()
    {
        //
    }

    public function onOpen(Server $server, Request $request)
    {
        $data = [
            'type' => 'login',
            'data' => Crypt::encryptString($request->fd)
        ];
        $server->push($request->fd, json_encode($data));
    }

    public function onMessage(Server $server, Frame $frame)
    {
        // \Log::info('Received message', [$frame->fd, $frame->data, $frame->opcode, $frame->finish]);
        // 此处抛出的异常会被上层捕获并记录到Swoole日志，开发者需要手动try/catch
        $server->push($frame->fd, date('Y-m-d H:i:s'));
    }

    public function onClose(Server $server, $fd, $reactorId)
    {
        // 此处抛出的异常会被上层捕获并记录到Swoole日志，开发者需要手动try/catch
    }
}
