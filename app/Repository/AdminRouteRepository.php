<?php
/**
 * Created by Raghu Liu
 * Date: 2022/9/17
 * Time: 13:37
 */

namespace App\Repository;

use App\Core\BaseRepository;
use App\Models\AdminRoute;

class AdminRouteRepository extends BaseRepository
{
    public function model()
    {
        return AdminRoute::class;
    }

    /**
     * @Desc: 后台查询菜单列表
     * @return mixed
     * @author: Raghu Liu
     * @Time: 2023/9/9 13:50
     */
    public function adminQuery()
    {
        return $this->query(
            $this->model->where('show', 1)->where('parent_id', 0)->with(['children'])->orderBy('order')
        );
    }

    /**
     * @Desc: 获取选项
     * @return mixed
     * @author: Raghu Liu
     * @Time: 2023/9/9 13:51
     */
    public function getOptions()
    {
        return $this->model->select(['admin_route_id as value', 'title as name'])->where('show', 1)->where('parent_id', 0)->get();
    }

    /**
     * @Desc: 设置权限时获取权限列表
     * @return mixed
     * @author: Raghu Liu
     * @Time: 2023/5/13 12:37
     */
    public function getListSetPermission()
    {
        return $this->model->select('*')
            ->with(['children' => function ($query) {
                $query->select('*')->with(['permission'])
                    ->where('show', 1)->orderBy('order', 'asc');
            }])
            ->where('parent_id', 0)
            ->where('show', 1)
            ->orderBy('order', 'asc')
            ->get();
    }

    /**
     * @Desc: 设置权限时获取权限列表
     * @return mixed
     * @author: Raghu Liu
     * @Time: 2023/5/13 12:37
     */
    public function getRouteList()
    {
        return $this->model->select('*')
            ->with(['children' => function ($query) {
                $query->select('*')->with(['permission'])->orderBy('order', 'asc');
            }])
            ->where('parent_id', 0)
            ->orderBy('order', 'asc')
            ->get();
    }

}
