<?php
/**
 * Created by Raghu Liu
 * Date: 2022/9/17
 * Time: 13:37
 */

namespace App\Repository;

use App\Core\BaseRepository;
use App\Core\PasswordEncrypt;
use App\Exceptions\ApiException;
use App\Models\Admin;
use Illuminate\Support\Str;

class AdminRepository extends BaseRepository
{
    public function model()
    {
        return Admin::class;
    }

    /**
     * @Desc: 根据用户名查询管理员
     * @param string $username
     * @return mixed
     * @author: Raghu Liu
     * @Time: 2023/5/12 21:36
     */
    public function getByUsername(string $username)
    {
        return $this->model->where('username', $username)->first();
    }

    /*
     * 获取信息
     */
    public function getInfo($id)
    {
        return $this->model
            ->select([
                'admin_id',
                'avatar_image_id',
                'username',
                'name',
                'last_login_at',
            ])
            ->with(['avatar'])
            ->find($id);
    }

    /**
     * @Desc:添加管理员
     * @param $username
     * @param $password
     * @param $name
     * @param $avatarImageID
     * @param $status
     * @param $super
     * @return mixed
     * @throws ApiException
     * @author: Raghu Liu
     * @Time: 2022/10/23 18:50
     */
    public function addAdmin($username, $password, $name, $avatarImageID, $status, $super)
    {
        $password = PasswordEncrypt::encrypt($password);

        return $this->create([
            'username'        => $username,
            'password'        => $password,
            'name'            => $name,
            'avatar_image_id' => $avatarImageID,
            'status'          => $status,
            'super'           => $super
        ]);

    }

    /**
     * @Desc:编辑管理员
     * @param $adminID
     * @param $password
     * @param $name
     * @param $avatarImageID
     * @param $status
     * @param $super
     * @return mixed
     * @throws ApiException
     * @author: Raghu Liu
     * @Time: 2022/10/23 18:50
     */
    public function editAdmin($admin_id, $password, $name, $avatar_image_id, $status, $super)
    {
        $data = [
            'name'            => $name,
            'avatar_image_id' => $avatar_image_id,
            'status'          => $status,
            'super'           => $super
        ];

        /* 判断是否修改了密码 */
        if ($password !== null) {
            $data['password'] = PasswordEncrypt::encrypt($password);
        }

        return $this->update($data, $admin_id);
    }

    /**
     * @Desc: 修改自己的信息
     * @param $adminId
     * @param $password
     * @param $avatarImageId
     * @return mixed
     * @throws ApiException
     * @author: Raghu Liu
     * @Time: 2023/3/12 11:21
     */
    public function editMyInfo($adminId, $password, $avatarImageId)
    {
        $data = [
            'avatar_image_id' => $avatarImageId,
        ];

        /* 判断是否修改了密码 */
        if ($password !== null) {
            $data['password'] = PasswordEncrypt::encrypt($password);
        }

        return $this->update($data, $adminId);
    }


    /**
     * 获取管理员列表
     */
    public function getList()
    {
        $search = $this->searchParam;
        $query  = $this->model
            ->select([
                'admin_id',
                'avatar_image_id',
                'username',
                'name',
                'super',
                'status',
                'admin_group_ids',
                'last_login_at',
                'last_login_ip',
                'created_at'
            ])
            ->with(['avatar'])
            ->with('roles')
            ->when($search->username, function ($query) use ($search) {
                $query->where('username', 'like', '%' . $search->username . '%');
            })
            ->when($search->status || is_numeric($search->status), function ($query) use ($search) {
                $query->where('status', $search->status);
            })
            ->when($search->status || is_array($search->status), function ($query) use ($search) {
                $query->whereIn('status', $search->status);
            });

        return $this->query($query);
    }

    public function getOptions()
    {
        $list = $this->model
            ->select([
                'admin_id as value',
                'name as label',
                'avatar_image_id'
            ])
            ->with(['avatar'])
            ->get();

        return $list;
    }

    /**
     * @Desc: 删除管理员
     * @author: Raghu Liu
     * @Time: 2024/2/8 15:53
     */
    public function delete($id)
    {
        $admin = $this->model->find($id);

        if ($admin->super) {
            throw new ApiException('超级管理员不能删除');
        }

        return $admin->delete();
    }
}
