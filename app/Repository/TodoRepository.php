<?php
/**
 * Created by Raghu Liu
 * Date: 2022/9/17
 * Time: 13:37
 */

namespace App\Repository;

use App\Core\BaseRepository;
use App\Models\Todo;

class TodoRepository extends BaseRepository
{
    public function model()
    {
        return Todo::class;
    }

    public function getMyList($adminID)
    {
        return $this->model->where('assignto_admin_id', $adminID)->get();
    }
}
