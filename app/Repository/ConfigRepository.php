<?php

namespace App\Repository;

use App\Core\BaseRepository;
use App\Models\Config;

class ConfigRepository extends BaseRepository
{
    public function model()
    {
        return Config::class;
    }

    public function adminQuery()
    {
        $query = $this->model
            ->with(['image'])
            ->with(['video']);
        return $query->get();
    }

    public function save($data)
    {
        $allowed = [
            'logo_image_id',
            'app_name',
            'video_id'
        ];

        foreach ($data as $name => $item) {
            if (!in_array($name, $allowed)) continue;

            $config = $this->model->where('name', $name)->first();
            if (!$config) {
                $config = new Config();
            }

            $config->name  = $name;
            $config->value = $item;
            $config->save();
        }
    }
}
