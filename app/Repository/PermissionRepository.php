<?php
/**
 * Created by Raghu Liu
 * Date: 2022/9/17
 * Time: 13:37
 */

namespace App\Repository;

use App\Core\BaseRepository;
use App\Models\Permission;
use App\Models\Todo;

class PermissionRepository extends BaseRepository
{
    public function model()
    {
        return Permission::class;
    }

    public function getByControl($control)
    {
        return $this->model->where('control', $control)->get();
    }
}
