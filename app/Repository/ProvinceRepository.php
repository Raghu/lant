<?php

namespace App\Repository;

use App\Core\BaseRepository;
use App\Models\Province;

class ProvinceRepository extends BaseRepository
{
    public function model()
    {
        return Province::class;
    }

    public function getPairs()
    {
        return $this->model->select(['province_id as value', 'name'])->get();
    }

    public function getTree($level = 1)
    {
        return $this->model
            ->select([
                'province_id', 'name', 'province_id as value', 'name as label',
            ])
            ->with(['children' => function ($query) use ($level) {
                $query
                    ->select([
                        'city_id', 'province_id', 'city_id as value', 'name as label', 'name'
                    ])
                    ->when($level == 2, function ($query) {
                        $query->with(['children' => function ($query) {
                            $query->select([
                                'district_id', 'city_id', 'district_id as value', 'name as label', 'name'
                            ]);
                        }]);
                    });
            }])
            ->get();
    }

}
