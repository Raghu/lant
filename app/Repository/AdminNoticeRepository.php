<?php
/**
 * Created by Raghu Liu
 * Date: 2023/9/17
 * Time: 9:53
 */

namespace App\Repository;

use App\Core\AdminAuth;
use App\Core\BaseRepository;
use App\Models\AdminNotice;

class AdminNoticeRepository extends BaseRepository
{
    public function model()
    {
        return AdminNotice::class;
    }

    public function getMy()
    {
        $admin_id = AdminAuth::id();

        return $this->model->where('admin_id', $admin_id)->with(['sendAdmin' => function($query){
            $query->masking();
        }])->limit(5)->get();
    }
}
