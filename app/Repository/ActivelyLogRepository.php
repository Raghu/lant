<?php

namespace App\Repository;

use App\Core\BaseRepository;
use App\Models\ActivelyLog;

class ActivelyLogRepository extends BaseRepository
{
    public function model()
    {
        return ActivelyLog::class;
    }

    public function adminQuery()
    {
        $query = $this->model->with(['causer' => function ($query) {
            return $query->masking();
        }]);

        return $this->query($query);
    }
}
