<?php
/**
 * Created by Raghu Liu
 * Date: 2022/9/17
 * Time: 13:37
 */

namespace App\Repository;

use App\Core\BaseRepository;
use App\Models\Log;
use App\Models\Todo;

class LogRepository extends BaseRepository
{
    public function model()
    {
        return Log::class;
    }

    public function getList()
    {
        return $this->model->with(['admin'=> function($query){
            $query->with(['avatar']);
        }, 'object'])->get();
    }
}
