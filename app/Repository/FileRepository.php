<?php
/**
 * Created by Raghu Liu
 * Date: 2023/9/17
 * Time: 9:53
 */

namespace App\Repository;

use App\Core\BaseRepository;
use App\Models\File;

class FileRepository extends BaseRepository
{
    public function model()
    {
        return File::class;
    }
}
