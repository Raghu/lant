<?php
/**
 * Created by Raghu Liu
 * Date: 2022/9/17
 * Time: 13:37
 */

namespace App\Repository;

use App\Core\BaseRepository;
use App\Models\AdminRole;

class AdminRoleRepository extends BaseRepository
{
    public function model()
    {
        return AdminRole::class;
    }

    public $createRule = [
        'name'=>'required'
    ];

    protected $createMessage = [
        'name.required'=>'请输入权限组名称'
    ];

    /**
     * @Desc: 查询角色列表
     * @return mixed
     * @author: Raghu Liu
     * @Time: 2023/5/13 12:41
     */
    public function getList()
    {
        return $this->query($this->model->select('*'));
    }

    /**
     * @Desc: 获取选项
     * @return mixed
     * @author: Raghu Liu
     * @Time: 2023/5/13 12:38
     */
    public function getPairs()
    {
        return $this->model->select(['id as value', 'name'])->get();
    }
}
