<?php
namespace App\Repository;

use App\Core\BaseRepository;
use App\Models\AdminLoginLog;

class AdminLoginLogRepository extends BaseRepository
{
    public function model()
    {
        return AdminLoginLog::class;
    }

    public function adminQuery()
    {
        $search = $this->searchParam;
        $query = $this->model
            ->when($search->adminId, function ($query) use ($search) {
                $query->where('admin_id',$search->adminId);
            });
        return $this->query($query);
    }

}
