<?php

namespace App\Repository;

use App\Core\BaseRepository;
use App\Models\Image;

class ImageRepository extends BaseRepository
{
    public function model()
    {
        return Image::class;
    }
}
