<?php

namespace App\Repository;

use App\Core\AdminAuth;
use App\Core\BaseRepository;
use App\Models\AdminOperationLog;

class AdminOperationLogRepository extends BaseRepository
{
    public function model()
    {
        return AdminOperationLog::class;
    }

    public function adminQuery()
    {
        $search = $this->searchParam;
        $query = $this->model
            ->with(['admin' => function($query){
                $query->masking();
            }, 'activityLog'])
            ->when($search->adminId, function ($query) use ($search) {
                $query->where('admin_id', $search->adminId);
            });
        return $this->query($query);
    }

}
