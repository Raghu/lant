<?php

namespace App\Cache;

use App\Core\BaseCache;
use App\Core\RedisCache;

class SmsCodeCache  extends BaseCache
{
    public static $code = 'smscode';
    public static $name = '短信验证码';
    public static $desc = '用于短信验证码的缓存';
    public static $type = 'string';

    protected static function getKey($scenario, $phone)
    {
        return 'smscode:scenario:' . $scenario . ':mobile:' . $phone;
    }

    public static function set($scenario, $phone, $code)
    {
        $key = self::getKey($scenario, $phone);
        RedisCache::SETEX($key, 60 * 5, $code);
    }

    public static function get($scenario, $phone)
    {
        $key = self::getKey($scenario, $phone);
        return RedisCache::get($key);
    }

    public static function allKeys()
    {
        $pattern = 'smscode:scenario:*:mobile:*';
        $keys = RedisCache::getKeys($pattern);

        return $keys;
    }
}
