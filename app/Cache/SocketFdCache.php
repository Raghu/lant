<?php

namespace App\Cache;

use App\Core\BaseCache;
use App\Core\RedisCache;

class SocketFdCache  extends BaseCache
{
    public static $code = 'socketid';
    public static $name = 'Socket ID';
    public static $desc = '用于记录socket连接的fd';
    public static $type = 'string';

    protected static function getKey($admin_id)
    {
        return 'socketid:admin_id:' . $admin_id;
    }

    public static function set($admin_id, $value)
    {
        $key = self::getKey($admin_id);
        return RedisCache::set($key, $value);
    }

    public static function get($admin_id)
    {
        $key = self::getKey($admin_id);
        return RedisCache::get($key);
    }

    public static function allKeys()
    {
        $pattern = 'socketid:admin_id:*';
        $keys = RedisCache::getKeys($pattern);

        return $keys;
    }
}
