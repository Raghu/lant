<?php

namespace App\Exceptions;

class CodeException extends \Exception
{

    function __construct($msg = '',$code = 40009)
    {
        parent::__construct($msg,$code);
    }
}
