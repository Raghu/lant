<?php

namespace App\Exceptions;

use Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Auth\AuthenticationException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Throwable $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, \Throwable $exception)
    {
        if ($exception instanceof HttpException) { //http请求错误
            if ($exception->getStatusCode() == 404) {
                $content = [
                    'status' => 404,
                    'msg' => '接口地址未找到'
                ];
            } elseif ($exception->getStatusCode() == 405) {
                $content = [
                    'status' => 405,
                    'msg' => '请求方式不允许'
                ];
            } else {
                $content = [
                    'status' => 500,
                    'msg' => '请求错误请重试'
                ];
            }
        } else {
            if ($exception instanceof ApiException) { //接口主动抛出
                $code = $exception->getCode();
                $msg = $exception->getMessage();

                $content = [
                    'status' => $code,
                    'msg' => json_decode($msg) ? json_decode($msg) : $msg
                ];

            }else {
                if (env('APP_DEBUG')) {
                    return parent::render($request, $exception);
                } else {
                    $content = [
                        'status' => 500,
                        'msg' => '请求错误请重试'
                    ];
                }

            }
        }


        return response()->json($content);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['status' => 401, 'message' => '登录已过期']);
        }

        return redirect()->guest($exception->redirectTo());
    }
}
