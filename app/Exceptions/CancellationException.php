<?php

namespace App\Exceptions;

class CancellationException extends ApiException
{
    function __construct($msg = '账户违规已封禁 禁止登录',$code = 40109)
    {
        parent::__construct($msg,$code);
    }
}