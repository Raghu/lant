<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/1/15
 * Time: 10:17
 */

namespace App\Exceptions;

class NoTokenException extends ApiException
{
    function __construct($msg='请先登录',$code=40101)
    {
        parent::__construct($msg,$code);
    }
}
