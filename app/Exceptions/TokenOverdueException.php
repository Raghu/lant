<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/1/15
 * Time: 10:17
 */

namespace App\Exceptions;

class TokenOverdueException extends ApiException
{
    function __construct($msg='登录已过期 请重新登录',$code=40102)
    {
        parent::__construct($msg,$code);
    }
}
