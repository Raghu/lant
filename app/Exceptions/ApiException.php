<?php

namespace App\Exceptions;

class ApiException extends \Exception
{
    function __construct($msg = '', $code = 400)
    {
        parent::__construct($msg, $code);
    }
}
