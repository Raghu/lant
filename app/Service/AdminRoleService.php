<?php
/**
 * Created by Raghu Liu
 * Date: 2022/9/25
 * Time: 15:09
 */

namespace App\Service;

use App\Models\Admin;
use App\Repository\AdminRepository;
use App\Repository\AdminRoleRepository;
use App\Repository\AdminRouteRepository;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\PermissionRegistrar;

class AdminRoleService
{
    private $adminRoleRepository;
    private $adminRouteRepository;
    private $adminRepository;

    public function __construct(
        AdminRoleRepository  $adminRoleRepository,
        AdminRouteRepository $adminRouteRepository,
        AdminRepository      $adminRepository
    )
    {
        $this->adminRoleRepository  = $adminRoleRepository;
        $this->adminRouteRepository = $adminRouteRepository;
        $this->adminRepository      = $adminRepository;
    }

    /**
     * @Desc: 获取某角色当前的权限项并分组
     * @param int $adminRoleId
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @author: Raghu Liu
     * @Time: 2023/5/13 12:09
     */
    public function getPermissionGroup(int $adminRoleId)
    {
        app()->make(PermissionRegistrar::class)->forgetCachedPermissions();
        $adminRole   = $this->adminRoleRepository->find($adminRoleId);
        $permissions = $adminRole->permissions;

        $routeList = $this->adminRouteRepository->getListSetPermission()->each(function ($item) use ($permissions) {
            foreach ($item->children as $route) {
                $route->checked = !!($permissions->where('control', $route->name)->count());
                foreach ($route->permission as $action) {
                    $flag            = $permissions->where('id', $action->id)->count();
                    $action->checked = !!$flag;
                }
            }
            return $item;
        });

        return $routeList;
    }

    /**
     * @Desc: 获取菜单
     * @param Admin $admin
     * @return array
     * @author: Raghu Liu
     * @Time: 2023/5/14 13:43
     */
    public function getMenuList(Admin $admin)
    {
        $permissions = $admin->getAllPermissions();

        $routeList = $this->adminRouteRepository->getRouteList()->toArray();

        foreach ($routeList as $key => $item) {
            $routeList[$key]['children'] = collect($item['children'])->filter(function ($child) use ($permissions) {
                return $child['common'] == 1 ? true : !!$permissions->where('control', $child['name'])->count();
            })->values();
        }


        $menu = $this->orderMenu(array_values($routeList));

        return [$menu, array_column($permissions->toArray(), 'name')];
    }

    /**
     * @Desc: 排序并设置重定向位置
     * @param $menuList
     * @return array
     * @author: Raghu Liu
     * @Time: 2023/5/14 13:43
     */
    public function orderMenu($menuList)
    {
        foreach ($menuList as $key => $item) {
            if (isset($item['children'][0])) $menuList[$key]['redirect'] = $item['children'][0]['path'];
        }

        return $menuList;
    }

    /**
     * @Desc: 设置权限
     * @param int $adminRoleId
     * @param array $data
     * @return bool
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @author: Raghu Liu
     * @Time: 2023/5/13 12:26
     */
    public function setPermissionList(int $adminRoleId, array $data): bool
    {
        $adminRole = $this->adminRoleRepository->find($adminRoleId);
        $adminRole->syncPermissions($data);

        app()->make(PermissionRegistrar::class)->forgetCachedPermissions();

        return true;
    }

    /**
     * @Desc: 分配一个角色
     * @param $adminId
     * @param $roleId
     * @return bool
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @author: Raghu Liu
     * @Time: 2023/5/14 12:06
     */
    public function assignRole($adminId, $roleId)
    {
        $admin = $this->adminRepository->find($adminId);

        $admin->syncRoles($roleId);

        app()->make(PermissionRegistrar::class)->forgetCachedPermissions();

        return true;
    }
}
