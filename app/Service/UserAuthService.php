<?php
/**
 * Created by Raghu Liu
 * Date: 2023/5/12
 * Time: 21:27
 */

namespace App\Service;

use App\Core\PasswordEncrypt;
use App\Exceptions\ApiException;
use App\Models\Admin;
use App\Models\User;
use App\Repository\AdminRepository;

class UserAuthService
{
    public static function login($user): User
    {
        $user->tokens()->delete();
        $token = $user->createToken('user')->plainTextToken;

        $user->token = $token;
        return $user;
    }
}
