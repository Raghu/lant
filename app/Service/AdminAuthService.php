<?php
/**
 * Created by Raghu Liu
 * Date: 2023/5/12
 * Time: 21:27
 */

namespace App\Service;

use App\Cache\SocketFdCache;
use App\Core\PasswordEncrypt;
use App\Exceptions\ApiException;
use App\Models\Admin;
use App\Repository\AdminRepository;

class AdminAuthService
{
    private $adminRepository;

    public function __construct(AdminRepository $adminRepository)
    {
        $this->adminRepository = $adminRepository;
    }

    /**
     * @Desc: 登录
     * @param string $username
     * @param string $password
     * @param string $ip
     * @param $user_agent
     * @return Admin
     * @throws ApiException
     * @author: Raghu Liu
     * @Time: 2023/5/12 21:50
     */
    public function login(string $username, string $password, string $ip, $user_agent): Admin
    {
        $admin = $this->adminRepository->getByUsername($username);
        if (!$admin) {
            throw new ApiException('用户不存在');
        }
        if ($this->passwordEncryption($password) !== $admin->password) {
            throw new ApiException('密码错误');
        }
        $admin->tokens()->delete();
        $token = $admin->createToken('admin')->plainTextToken;
        $this->adminRepository->update(['last_login_ip' => $ip, 'last_login_at' => date('Y-m-d H:i:s')], $admin->admin_id);

        $admin->token = $token;
        return $admin;
    }


    /**
     * @Desc: 密码加密
     * @param $password
     * @return string
     * @author: Raghu Liu
     * @Time: 2023/5/12 21:51
     */
    private function passwordEncryption($password)
    {
        return PasswordEncrypt::encrypt($password);
    }

    /**
     * @Desc: 绑定socket id
     * @param $admin_id
     * @param $socket_id
     * @return bool
     * @throws ApiException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @throws \Throwable
     * @Time: 2023/5/12 21:51
     */
    public function bindSocketId($admin_id, $socket_id)
    {
        return SocketFdCache::set($admin_id, $socket_id);
    }
}
