<?php

namespace App\Service;

use App\Core\AdminAuth;
use App\Core\CaptureActivityLogCollection;
use App\Models\AdminOperationLog;

class LogService
{
    public static function add($title, $action)
    {
        $data = [
            'action'   => $action,
            'title'    => $title,
            'admin_id' => AdminAuth::id(),
            'ip'       => request()->getClientIp()
        ];

        $adminOperationLog = AdminOperationLog::create($data);

        $activityLogList = CaptureActivityLogCollection::getCollection();

        if ($activityLogList) {
            foreach ($activityLogList as $activityLog) {
                $activityLog->admin_operation_log_id = $adminOperationLog->admin_operation_log_id;
                $activityLog->save();
            }
        }

        CaptureActivityLogCollection::clear();
    }
}
