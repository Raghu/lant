<?php
/**
 * Created by Raghu Liu
 * Date: 2023/9/17
 * Time: 9:52
 */

namespace App\Service;

use App\Repository\FileRepository;

class FileService
{
    public static function getRepository()
    {
        return new FileRepository();
    }

    /**
     * @Desc: 保存文件
     * @param $dir
     * @param $file
     * @param $access
     * @return mixed
     * @throws \App\Exceptions\ApiException
     * @author: Raghu Liu
     * @Time: 2023/9/17 9:55
     */
    public static function saveFile($dir, $file, $access = 'public')
    {
        $path = $file->store($dir);

        return self::getRepository()->create(
            [
                'dir'            => $dir,
                'path'           => $path,
                'filename'       => $file->getClientOriginalName(),
                'access'         => $access,
                'file_extension' => $file->getClientOriginalExtension(),
                'size'           => $file->getSize(),
                'mime_type'      => $file->getMimeType(),
                'code'           => md5(time() . $path . uniqid())
            ]
        );
    }
}
