<?php

namespace App\Service;

use App\Models\Image;

class ImageService
{
    public static function saveImage($dir, $image, $access = 'public')
    {
        $path = $image->store($dir);

        return Image::create(
            [
                'dir'      => $dir,
                'path'     => $path,
                'filename' => $image->getClientOriginalName(),
                'access'   => $access,
                'code'     => md5(time() . $path . uniqid())
            ]
        );
    }
}
