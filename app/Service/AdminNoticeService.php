<?php
/**
 * Created by Raghu Liu
 * Date: 2023/9/24
 * Time: 12:16
 */

namespace App\Service;

use App\Cache\SocketFdCache;
use App\Repository\AdminNoticeRepository;
use Illuminate\Support\Facades\Log;

class AdminNoticeService
{
    public static function getRepository()
    {
        return app()->make('App\Repository\AdminNoticeRepository');
    }

    /**
     * @Desc: 发送通知
     * @param int $send_admin_id
     * @param int $admin_id
     * @param string $title
     * @param string $content
     * @return bool
     * @throws \App\Exceptions\ApiException
     * @author: Raghu Liu
     * @Time: 2023/9/24 12:26
     */
    public static function send(int $send_admin_id, int $admin_id, string $title, string $content): bool
    {
        $data = [
            'send_admin_id' => $send_admin_id,
            'admin_id'      => $admin_id,
            'title'         => $title,
            'content'       => $content
        ];

        $data = self::getRepository()->create($data);
        if (config('laravels.websocket.enable')) {
            $data->load(['sendAdmin' => function ($query) {
                $query->masking();
            }]);
            $data->is_read = 0;
            self::sendWebSocket($admin_id, 'notice', $data);
        }
        return true;
    }

    /**
     * @Desc: 发送websocket
     * @param $admin_id
     * @param $type
     * @param $data
     * @throws \App\Exceptions\ApiException
     * @author: Raghu Liu
     * @Time: 2023/9/24 12:26
     */
    public static function sendWebSocket($admin_id, $type, $data)
    {
        $data      = [
            'type' => $type,
            'data' => $data
        ];
        $socket_id = SocketFdCache::get($admin_id);
        $server    = app('swoole');
        $server->push($socket_id, json_encode($data));
    }
}
