<?php
/**
 * Created by Raghu Liu
 * Date: 2022/9/25
 * Time: 15:09
 */

namespace App\Service;

use App\Models\Permission;

class AdminPermissionService
{
    /**
     * @Desc: 设置权限列表
     * @param $control
     * @param $permissionList
     * @return bool
     * @author: Raghu Liu
     * @Time: 2023/9/17 14:48
     */
    public static function setPermissionList($control, $permissionList)
    {
        $currentPermissions = Permission::where('control', $control)->pluck('id')->toArray();

        $providedIds = [];
        foreach ($permissionList as $value) {
            isset($value['id']) ?: $value['id'] = 0;
            $permission = Permission::find($value['id']);
            if (!$permission) {
                $permission = new Permission();
            } else {
                $providedIds[] = $permission->id;
            }

            $permission->control    = $control;
            $permission->name       = $value['name'];
            $permission->desc       = $value['desc'];
            $permission->module     = $value['module'];
            $permission->guard_name = 'admin';
            $permission->save();
        }

        $permissionsToDelete = array_diff($currentPermissions, $providedIds);
        Permission::whereIn('id', $permissionsToDelete)->delete();

        return true;
    }
}
