<table>
    <thead>
    <tr>
        <th>交易流水号</th>
        <th>提现用户</th>
        <th>真实姓名</th>
        <th>提现金额</th>
        <th>手续费</th>
        <th>实际金额</th>
        <th>账户</th>
        <th>账户名</th>
        <th>开户行城市</th>
        <th>支行地址</th>
        <th>审核/打款时间</th>

    </tr>
    </thead>
    <tbody>
    @foreach($withdrawal as $v)
        <tr>
            <td>
               {{ $v->num }}
            </td>
            <td>
                {{ $v->user->mobile }}
            </td>
            <td>
                {{ $v->user->name }}
            </td>
            <td>{{ $v->number }}</td>
            <td>{{ $v->poundage }}</td>
            <td>{{ $v->actual_number }}</td>
            <td>{{ "'".$v->user_cash->account }}</td>
            <td>{{ $v->user_cash->name }}</td>
            <td>{{ $v->user_cash->province->name }} {{ $v->user_cash->city->name }}</td>
            <td>{{ $v->user_cash->address }}</td>
            <td>{{ $v->money_at }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
