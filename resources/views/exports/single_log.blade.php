<table>
    <thead>
    <tr>
        <th>会员账号</th>
        <th>订单号</th>
        <th>变动数量</th>
        <th>变动原因</th>
        <th>变动时间</th>
    </tr>
    </thead>
    <tbody>
    @foreach($single_log as $v)
        <tr>
            <td>{{ $v->user->mobile }}</td>
            <td>{{ $v->order->num}}</td>
            <td>{{ $v->single }}</td>
            <td>{{ $v->marker }}</td>
            <td>{{ $v->created_at }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
