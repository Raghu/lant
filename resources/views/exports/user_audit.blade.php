<table>
    <thead>
    <tr>
        <th>审核流水号</th>
        <th>会员ID</th>
        <th>店铺名称</th>
        <th>收货名称</th>
        <th>收货名称</th>
        <th>店铺电话</th>
        <th>发货状态</th>
        <th>审核状态</th>
        <th>创建时间</th>
    </tr>
    </thead>
    <tbody>
    @foreach($user_audit as $v)
        <tr>
            <td>{{ $v->num }}</td>
            <td>{{ $v->user_id }}</td>
            <td>{{ $v->name }}</td>
            <td>{{ $v->address_name }}</td>
            <td>
                @if(isset($v->province)) {{ $v->province->name }} @endif
                @if(isset($v->city)) {{ $v->city->name }} @endif
                @if(isset($v->district)){{ $v->district->name }} @endif
                @if(isset($v->address)) {{ $v->address }} @endif
            </td>
            <td>{{ $v->phone }}</td>
            <td>
                @if($v->courier_status == 0)
                    待发货
                @elseif($v->courier_status == 1)
                    已发货
                @endif
            </td>
            <td>
                @if($v->status == 0)
                    待审核
                @elseif($v->status == 1)
                    通过
                @elseif($v->status == 2)
                    驳回
                @endif
            </td>
            <td>{{ $v->created_at }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
