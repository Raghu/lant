<table>
    <thead>
    <tr>
        <th>订单号</th>
        <th>订单状态</th>
        <th>商品名称</th>
        <th>商品数量</th>
        <th>商品总价</th>
        <th>购买账号</th>
        <th>会员ID</th>
        <th>创建时间</th>
        <th>支付时间</th>
        <th>收货地址</th>
        <th>收货人</th>
        <th>收货人手机号</th>
        <th>备注</th>
        <th>支付总价</th>
        <th>订单单量</th>
    </tr>
    </thead>
    <tbody>
    @foreach($order as $v)
        @foreach($v->order_item as $value)
        <tr>
            <td>{{ $v->num }}</td>
            <td>
                @if($v->status == 0)
                    未支付
                @elseif($v->status == 1)
                    待发货
                @elseif($v->status == 2)
                    已发货
                @elseif($v->status == 3)
                    已完成
                @elseif($v->status == 4)
                    已取消
                @endif
            </td>
            <td>{{ $value->product->name }}</td>
            <td>{{ $value->number }}</td>
            <td>{{ $value->total_price }}</td>
            <td>{{ $v->user->mobile }}</td>
            <td>{{ $v->user_id }}</td>
            <td>{{ $v->created_at }}</td>
            <td>{{ $v->pay_at }}</td>
            <td>
                @if(isset($v->shipping_address))
                    @if(isset($v->shipping_address->province)) {{ $v->shipping_address->province->name }} @endif
                    @if(isset($v->shipping_address->city)) {{ $v->shipping_address->city->name }} @endif
                    @if(isset($v->shipping_address->district)){{ $v->shipping_address->district->name }} @endif
                    @if(isset($v->shipping_address)) {{ $v->shipping_address->address }} @endif
                @endif
            </td>
            <td>@if(isset($v->shipping_address)) {{ $v->shipping_address->name }} @endif </td>
            <td>@if(isset($v->shipping_address)) {{ $v->shipping_address->mobile }} @endif </td>
            <td>{{ $v->marker }}</td>
            <td>{{ $v->pay_price }}</td>
            <td>{{ $v->total_single }}</td>
        </tr>
        @endforeach
    @endforeach
    </tbody>
</table>
