<table>
    <thead>
    <tr>
        <th>变动类型</th>
        <th>变动状态</th>
        <th>流水号</th>
        <th>会员账号</th>
        <th>真实姓名</th>
        <th>会员身份</th>
        <th>变动数量</th>
        <th>实时数量</th>
        <th>变动原因</th>
        <th>变动时间</th>
    </tr>
    </thead>
    <tbody>
    @foreach($money_log as $v)
        <tr>
            <td>
                @if($v->type == 1)
                    推荐积分
                @elseif($v->type == 2)
                    积分提现
                @elseif($v->type == 3)
                    提现退回
                @elseif($v->type == 4)
                    抵扣订单
                @elseif($v->type == 5)
                    其他提成
                @elseif($v->type == 6)
                    区域代理提成
                @elseif($v->type == 7)
                    平级积分
                @elseif($v->type == 9)
                    后台充值
                @elseif($v->type == 10)
                    团队互转
                @elseif($v->type == 11)
                    股东提成
                @elseif($v->type == 12)
                    旧平台迁移
                @elseif($v->type == 13)
                    订货返还
                @elseif($v->type == 15)
                    后台核减
                @endif
            </td>
            <td>
                @if($v->status == 0)
                    增加
                @elseif($v->status == 1)
                    减少
                @endif
            </td>
            <td>{{ $v->serial_number }}</td>
            <td>{{ $v->user->mobile }}</td>
            <td>{{ $v->user->name }}</td>
            <td>{{ $v->user->user_member_config->name }}</td>
            <td>{{ $v->money }}</td>
            <td>{{ $v->user_money }}</td>
            <td>{{ $v->marker }}</td>
            <td>{{ $v->created_at }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
