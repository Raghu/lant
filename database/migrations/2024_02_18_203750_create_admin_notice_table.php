<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_notice', function (Blueprint $table) {
            $table->integer('admin_notice_id', true);
            $table->string('title')->nullable()->comment('通知标题');
            $table->string('content')->nullable()->comment('通知内容');
            $table->integer('send_admin_id')->nullable()->comment('发送方id');
            $table->integer('admin_id')->nullable()->comment('接收方id');
            $table->tinyInteger('is_read')->default(0)->comment('0-未读  1-已读');
            $table->dateTime('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_notice');
    }
};
