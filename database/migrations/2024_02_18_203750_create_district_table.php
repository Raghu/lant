<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('district', function (Blueprint $table) {
            $table->integer('district_id', true);
            $table->string('name')->nullable();
            $table->string('short_name')->nullable();
            $table->string('pinyin')->nullable();
            $table->string('first_char')->nullable();
            $table->string('lng')->nullable()->comment('经度');
            $table->string('lat')->nullable()->comment('纬度');
            $table->integer('province_id')->nullable()->comment('省份id');
            $table->integer('city_id')->nullable()->comment('城市id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('district');
    }
};
