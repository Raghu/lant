<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_login_log', function (Blueprint $table) {
            $table->integer('admin_login_log_id', true)->comment('员工后台登录日志');
            $table->integer('admin_id')->nullable()->comment('员工id');
            $table->string('browser', 30)->nullable()->comment('浏览器');
            $table->string('browser_version', 30)->nullable()->comment('浏览器版本');
            $table->string('device', 30)->nullable()->comment('设备信息');
            $table->string('platform', 30)->nullable()->comment('操作系统');
            $table->string('platform_version', 30)->nullable()->comment('操作系统版本');
            $table->string('login_ip', 30)->nullable()->comment('登录ip');
            $table->timestamp('created_at')->nullable()->comment('时间');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_login_log');
    }
};
