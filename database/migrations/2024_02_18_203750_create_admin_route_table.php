<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_route', function (Blueprint $table) {
            $table->increments('admin_route_id');
            $table->string('name', 191);
            $table->integer('parent_id');
            $table->string('title', 191);
            $table->unsignedTinyInteger('show')->default(1);
            $table->string('target', 191)->nullable();
            $table->unsignedTinyInteger('blank')->nullable();
            $table->string('path', 191);
            $table->string('src')->nullable();
            $table->string('component', 191);
            $table->string('redirect', 191)->nullable();
            $table->string('icon', 191)->nullable();
            $table->integer('order')->nullable()->default(0);
            $table->boolean('common')->default(false);
            $table->dateTime('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_route');
    }
};
