<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banner', function (Blueprint $table) {
            $table->integer('banner_id', true)->comment('banner表id');
            $table->integer('image_id')->comment('图片地址');
            $table->tinyInteger('location')->default(1)->comment('1 轮播图.  2分类轮播图');
            $table->integer('sort')->default(0)->comment('排序字段');
            $table->boolean('status')->default(true)->comment('0不显示  1显示');
            $table->timestamp('created_at');
            $table->integer('domain_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banner');
    }
};
