<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->integer('user_id', true);
            $table->string('nickname')->nullable()->comment('昵称');
            $table->string('mobile')->nullable()->comment('登录账号');
            $table->string('password')->nullable()->default('')->comment('密码');
            $table->boolean('status')->nullable()->default(false)->comment('会员状态 0正常 1禁用');
            $table->integer('avatar_image_id')->nullable()->default(1)->comment('头像');
            $table->boolean('sex')->nullable()->default(true)->comment('性别 1男 2女');
            $table->string('last_login_at')->nullable();
            $table->string('last_login_ip')->nullable();
            $table->timestamp('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
};
