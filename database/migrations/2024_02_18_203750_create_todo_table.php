<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('todo', function (Blueprint $table) {
            $table->integer('todo_id', true);
            $table->integer('admin_id')->nullable()->comment('创建人ID');
            $table->string('title')->nullable()->comment('待办标题');
            $table->timestamp('start')->nullable()->comment('开始时间');
            $table->timestamp('end')->nullable()->comment('结束时间');
            $table->boolean('multi_day')->nullable()->comment('是否跨天 0不跨天 1跨天');
            $table->boolean('all_day')->nullable()->comment('是否是全天性待办 0不是 1是');
            $table->integer('assignto_admin_id')->nullable();
            $table->text('desc')->nullable()->comment('待办描述');
            $table->timestamp('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('todo');
    }
};
