<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image', function (Blueprint $table) {
            $table->integer('image_id', true);
            $table->string('url')->nullable();
            $table->string('code')->nullable();
            $table->string('path')->nullable();
            $table->string('dir')->nullable();
            $table->string('filename')->nullable();
            $table->enum('access', ['private', 'protected', 'public'])->nullable()->default('public');
            $table->dateTime('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('image');
    }
};
