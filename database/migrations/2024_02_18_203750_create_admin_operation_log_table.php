<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_operation_log', function (Blueprint $table) {
            $table->integer('admin_operation_log_id', true);
            $table->integer('admin_id')->nullable();
            $table->string('title')->nullable();
            $table->string('action')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->string('ip')->nullable();
            $table->integer('domain_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_operation_log');
    }
};
