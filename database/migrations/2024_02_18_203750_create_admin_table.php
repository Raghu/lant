<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin', function (Blueprint $table) {
            $table->integer('admin_id', true);
            $table->string('username')->nullable();
            $table->string('password')->nullable();
            $table->string('salt')->nullable();
            $table->string('name')->nullable();
            $table->integer('avatar_image_id')->nullable()->default(1);
            $table->string('token')->nullable();
            $table->string('token_expire')->nullable();
            $table->string('last_login_ip')->nullable();
            $table->dateTime('last_login_at')->nullable();
            $table->tinyInteger('status')->nullable()->comment('是否启用');
            $table->dateTime('created_at')->nullable();
            $table->tinyInteger('super')->nullable()->default(0)->comment('1为超级管理员');
            $table->string('admin_group_ids')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin');
    }
};
