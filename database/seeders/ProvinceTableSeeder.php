<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ProvinceTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('province')->delete();
        
        \DB::table('province')->insert(array (
            0 => 
            array (
                'province_id' => 110000,
                'name' => '北京',
                'short_name' => '北京',
                'pinyin' => 'Beijing',
                'first_char' => 'B',
                'lng' => '116.405285',
                'lat' => '39.904989',
            ),
            1 => 
            array (
                'province_id' => 120000,
                'name' => '天津',
                'short_name' => '天津',
                'pinyin' => 'Tianjin',
                'first_char' => 'T',
                'lng' => '117.190182',
                'lat' => '39.125596',
            ),
            2 => 
            array (
                'province_id' => 130000,
                'name' => '河北省',
                'short_name' => '河北',
                'pinyin' => 'Hebei',
                'first_char' => 'H',
                'lng' => '114.502461',
                'lat' => '38.045474',
            ),
            3 => 
            array (
                'province_id' => 140000,
                'name' => '山西省',
                'short_name' => '山西',
                'pinyin' => 'Shanxi',
                'first_char' => 'S',
                'lng' => '112.549248',
                'lat' => '37.857014',
            ),
            4 => 
            array (
                'province_id' => 150000,
                'name' => '内蒙古',
                'short_name' => '内蒙古',
                'pinyin' => 'Inner Mongolia',
                'first_char' => 'I',
                'lng' => '111.670801',
                'lat' => '40.818311',
            ),
            5 => 
            array (
                'province_id' => 210000,
                'name' => '辽宁省',
                'short_name' => '辽宁',
                'pinyin' => 'Liaoning',
                'first_char' => 'L',
                'lng' => '123.429096',
                'lat' => '41.796767',
            ),
            6 => 
            array (
                'province_id' => 220000,
                'name' => '吉林省',
                'short_name' => '吉林',
                'pinyin' => 'Jilin',
                'first_char' => 'J',
                'lng' => '125.3245',
                'lat' => '43.886841',
            ),
            7 => 
            array (
                'province_id' => 230000,
                'name' => '黑龙江省',
                'short_name' => '黑龙江',
                'pinyin' => 'Heilongjiang',
                'first_char' => 'H',
                'lng' => '126.642464',
                'lat' => '45.756967',
            ),
            8 => 
            array (
                'province_id' => 310000,
                'name' => '上海',
                'short_name' => '上海',
                'pinyin' => 'Shanghai',
                'first_char' => 'S',
                'lng' => '121.472644',
                'lat' => '31.231706',
            ),
            9 => 
            array (
                'province_id' => 320000,
                'name' => '江苏省',
                'short_name' => '江苏',
                'pinyin' => 'Jiangsu',
                'first_char' => 'J',
                'lng' => '118.767413',
                'lat' => '32.041544',
            ),
            10 => 
            array (
                'province_id' => 330000,
                'name' => '浙江省',
                'short_name' => '浙江',
                'pinyin' => 'Zhejiang',
                'first_char' => 'Z',
                'lng' => '120.153576',
                'lat' => '30.287459',
            ),
            11 => 
            array (
                'province_id' => 340000,
                'name' => '安徽省',
                'short_name' => '安徽',
                'pinyin' => 'Anhui',
                'first_char' => 'A',
                'lng' => '117.283042',
                'lat' => '31.86119',
            ),
            12 => 
            array (
                'province_id' => 350000,
                'name' => '福建省',
                'short_name' => '福建',
                'pinyin' => 'Fujian',
                'first_char' => 'F',
                'lng' => '119.306239',
                'lat' => '26.075302',
            ),
            13 => 
            array (
                'province_id' => 360000,
                'name' => '江西省',
                'short_name' => '江西',
                'pinyin' => 'Jiangxi',
                'first_char' => 'J',
                'lng' => '115.892151',
                'lat' => '28.676493',
            ),
            14 => 
            array (
                'province_id' => 370000,
                'name' => '山东省',
                'short_name' => '山东',
                'pinyin' => 'Shandong',
                'first_char' => 'S',
                'lng' => '117.000923',
                'lat' => '36.675807',
            ),
            15 => 
            array (
                'province_id' => 410000,
                'name' => '河南省',
                'short_name' => '河南',
                'pinyin' => 'Henan',
                'first_char' => 'H',
                'lng' => '113.665412',
                'lat' => '34.757975',
            ),
            16 => 
            array (
                'province_id' => 420000,
                'name' => '湖北省',
                'short_name' => '湖北',
                'pinyin' => 'Hubei',
                'first_char' => 'H',
                'lng' => '114.298572',
                'lat' => '30.584355',
            ),
            17 => 
            array (
                'province_id' => 430000,
                'name' => '湖南省',
                'short_name' => '湖南',
                'pinyin' => 'Hunan',
                'first_char' => 'H',
                'lng' => '112.982279',
                'lat' => '28.19409',
            ),
            18 => 
            array (
                'province_id' => 440000,
                'name' => '广东省',
                'short_name' => '广东',
                'pinyin' => 'Guangdong',
                'first_char' => 'G',
                'lng' => '113.280637',
                'lat' => '23.125178',
            ),
            19 => 
            array (
                'province_id' => 450000,
                'name' => '广西',
                'short_name' => '广西',
                'pinyin' => 'Guangxi',
                'first_char' => 'G',
                'lng' => '108.320004',
                'lat' => '22.82402',
            ),
            20 => 
            array (
                'province_id' => 460000,
                'name' => '海南省',
                'short_name' => '海南',
                'pinyin' => 'Hainan',
                'first_char' => 'H',
                'lng' => '110.33119',
                'lat' => '20.031971',
            ),
            21 => 
            array (
                'province_id' => 500000,
                'name' => '重庆',
                'short_name' => '重庆',
                'pinyin' => 'Chongqing',
                'first_char' => 'C',
                'lng' => '106.504962',
                'lat' => '29.533155',
            ),
            22 => 
            array (
                'province_id' => 510000,
                'name' => '四川省',
                'short_name' => '四川',
                'pinyin' => 'Sichuan',
                'first_char' => 'S',
                'lng' => '104.065735',
                'lat' => '30.659462',
            ),
            23 => 
            array (
                'province_id' => 520000,
                'name' => '贵州省',
                'short_name' => '贵州',
                'pinyin' => 'Guizhou',
                'first_char' => 'G',
                'lng' => '106.713478',
                'lat' => '26.578343',
            ),
            24 => 
            array (
                'province_id' => 530000,
                'name' => '云南省',
                'short_name' => '云南',
                'pinyin' => 'Yunnan',
                'first_char' => 'Y',
                'lng' => '102.712251',
                'lat' => '25.040609',
            ),
            25 => 
            array (
                'province_id' => 540000,
                'name' => '西藏',
                'short_name' => '西藏',
                'pinyin' => 'Tibet',
                'first_char' => 'T',
                'lng' => '91.132212',
                'lat' => '29.660361',
            ),
            26 => 
            array (
                'province_id' => 610000,
                'name' => '陕西省',
                'short_name' => '陕西',
                'pinyin' => 'Shaanxi',
                'first_char' => 'S',
                'lng' => '108.948024',
                'lat' => '34.263161',
            ),
            27 => 
            array (
                'province_id' => 620000,
                'name' => '甘肃省',
                'short_name' => '甘肃',
                'pinyin' => 'Gansu',
                'first_char' => 'G',
                'lng' => '103.823557',
                'lat' => '36.058039',
            ),
            28 => 
            array (
                'province_id' => 630000,
                'name' => '青海省',
                'short_name' => '青海',
                'pinyin' => 'Qinghai',
                'first_char' => 'Q',
                'lng' => '101.778916',
                'lat' => '36.623178',
            ),
            29 => 
            array (
                'province_id' => 640000,
                'name' => '宁夏',
                'short_name' => '宁夏',
                'pinyin' => 'Ningxia',
                'first_char' => 'N',
                'lng' => '106.278179',
                'lat' => '38.46637',
            ),
            30 => 
            array (
                'province_id' => 650000,
                'name' => '新疆',
                'short_name' => '新疆',
                'pinyin' => 'Xinjiang',
                'first_char' => 'X',
                'lng' => '87.617733',
                'lat' => '43.792818',
            ),
            31 => 
            array (
                'province_id' => 710000,
                'name' => '台湾',
                'short_name' => '台湾',
                'pinyin' => 'Taiwan',
                'first_char' => 'T',
                'lng' => '121.509062',
                'lat' => '25.044332',
            ),
            32 => 
            array (
                'province_id' => 810000,
                'name' => '香港',
                'short_name' => '香港',
                'pinyin' => 'Hong Kong',
                'first_char' => 'H',
                'lng' => '114.173355',
                'lat' => '22.320048',
            ),
            33 => 
            array (
                'province_id' => 820000,
                'name' => '澳门',
                'short_name' => '澳门',
                'pinyin' => 'Macau',
                'first_char' => 'M',
                'lng' => '113.54909',
                'lat' => '22.198951',
            ),
            34 => 
            array (
                'province_id' => 900000,
                'name' => '钓鱼岛',
                'short_name' => '钓鱼岛',
                'pinyin' => 'DiaoyuDao',
                'first_char' => 'D',
                'lng' => '123.478088',
                'lat' => '25.742385',
            ),
        ));
        
        
    }
}