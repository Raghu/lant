<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ActivityLogTableSeeder::class);
        $this->call(AdminTableSeeder::class);
        $this->call(AdminLoginLogTableSeeder::class);
        $this->call(AdminNoticeTableSeeder::class);
        $this->call(AdminOperationLogTableSeeder::class);
        $this->call(AdminRouteTableSeeder::class);
        $this->call(BannerTableSeeder::class);
        $this->call(CityTableSeeder::class);
        $this->call(ConfigTableSeeder::class);
        $this->call(DistrictTableSeeder::class);
        $this->call(FileTableSeeder::class);
        $this->call(ImageTableSeeder::class);
        $this->call(LogTableSeeder::class);
        $this->call(ModelHasPermissionsTableSeeder::class);
        $this->call(ModelHasRolesTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(PersonalAccessTokensTableSeeder::class);
        $this->call(ProvinceTableSeeder::class);
        $this->call(RoleHasPermissionsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(TodoTableSeeder::class);
        $this->call(UserTableSeeder::class);
    }
}
