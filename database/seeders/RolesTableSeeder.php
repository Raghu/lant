<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => '总管理员',
                'guard_name' => 'admin',
                'desc' => '拥有所有权限',
                'created_at' => '2023-05-13 01:42:46',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => '测',
                'guard_name' => 'admin',
                'desc' => NULL,
                'created_at' => '2023-11-05 12:41:22',
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}