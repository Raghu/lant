<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('admin')->delete();
        
        \DB::table('admin')->insert(array (
            0 => 
            array (
                'admin_id' => 3,
                'username' => 'admin',
                'password' => '06e2569b2dbe308c76cb88f09a7ac5e6',
                'salt' => NULL,
                'name' => '超级管理员',
                'avatar_image_id' => 251,
                'token' => NULL,
                'token_expire' => NULL,
                'last_login_ip' => '192.168.33.1',
                'last_login_at' => '2024-02-08 15:40:58',
                'status' => 1,
                'created_at' => '2023-09-09 12:57:25',
                'super' => 1,
                'admin_group_ids' => NULL,
            ),
            1 => 
            array (
                'admin_id' => 6,
                'username' => '1231231',
                'password' => '06e2569b2dbe308c76cb88f09a7ac5e6',
                'salt' => NULL,
                'name' => '1231231522222222',
                'avatar_image_id' => 247,
                'token' => NULL,
                'token_expire' => NULL,
                'last_login_ip' => NULL,
                'last_login_at' => NULL,
                'status' => 1,
                'created_at' => '2023-09-23 21:08:41',
                'super' => 0,
                'admin_group_ids' => NULL,
            ),
        ));
        
        
    }
}