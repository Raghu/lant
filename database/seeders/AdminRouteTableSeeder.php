<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AdminRouteTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('admin_route')->delete();
        
        \DB::table('admin_route')->insert(array (
            0 => 
            array (
                'admin_route_id' => 1,
                'name' => 'Dashboard',
                'parent_id' => 0,
                'title' => '首页',
                'show' => 1,
                'target' => '',
                'blank' => 0,
                'path' => '/dashboard',
                'src' => NULL,
                'component' => 'RouteView',
                'redirect' => '/dashboard/workplace',
                'icon' => 'home-outlined',
                'order' => 1,
                'common' => 1,
                'created_at' => NULL,
            ),
            1 => 
            array (
                'admin_route_id' => 2,
                'name' => 'Workplace',
                'parent_id' => 1,
                'title' => '工作台',
                'show' => 1,
                'target' => '',
                'blank' => 0,
                'path' => '/dashboard/workplace',
                'src' => NULL,
                'component' => '/dashboard/workplace/index.vue',
                'redirect' => NULL,
                'icon' => '',
                'order' => NULL,
                'common' => 1,
                'created_at' => NULL,
            ),
            2 => 
            array (
                'admin_route_id' => 3,
                'name' => 'Todo',
                'parent_id' => 1,
                'title' => '日程',
                'show' => 1,
                'target' => '',
                'blank' => 0,
                'path' => '/dashboard/todo',
                'src' => NULL,
                'component' => '/dashboard/todo/index.vue',
                'redirect' => NULL,
                'icon' => '',
                'order' => 3,
                'common' => 1,
                'created_at' => NULL,
            ),
            3 => 
            array (
                'admin_route_id' => 4,
                'name' => 'Info',
                'parent_id' => 1,
                'title' => '修改信息',
                'show' => 0,
                'target' => '',
                'blank' => 0,
                'path' => '/dashboard/user-info',
                'src' => NULL,
                'component' => '/dashboard/userinfo/index.vue',
                'redirect' => NULL,
                'icon' => '',
                'order' => NULL,
                'common' => 1,
                'created_at' => NULL,
            ),
            4 => 
            array (
                'admin_route_id' => 5,
                'name' => 'Setting',
                'parent_id' => 0,
                'title' => '系统设置',
                'show' => 1,
                'target' => '',
                'blank' => 0,
                'path' => '/setting',
                'src' => NULL,
                'component' => 'RouteView',
                'redirect' => '/setting/admin',
                'icon' => 'setting-outlined',
                'order' => 100,
                'common' => 0,
                'created_at' => NULL,
            ),
            5 => 
            array (
                'admin_route_id' => 6,
                'name' => 'Admin',
                'parent_id' => 5,
                'title' => '管理员设置',
                'show' => 1,
                'target' => '',
                'blank' => 0,
                'path' => '/setting/admin',
                'src' => NULL,
                'component' => '/setting/admin/index.vue',
                'redirect' => '',
                'icon' => '',
                'order' => NULL,
                'common' => 0,
                'created_at' => NULL,
            ),
            6 => 
            array (
                'admin_route_id' => 7,
                'name' => 'AdminRole',
                'parent_id' => 5,
                'title' => '角色设置',
                'show' => 1,
                'target' => '',
                'blank' => 0,
                'path' => '/setting/admin-role',
                'src' => NULL,
                'component' => '/setting/adminrole/index.vue',
                'redirect' => NULL,
                'icon' => '',
                'order' => NULL,
                'common' => 0,
                'created_at' => NULL,
            ),
            7 => 
            array (
                'admin_route_id' => 49,
                'name' => 'Dev',
                'parent_id' => 0,
                'title' => '开发者工具',
                'show' => 1,
                'target' => NULL,
                'blank' => 0,
                'path' => '/dev',
                'src' => NULL,
                'component' => 'RouteView',
                'redirect' => NULL,
                'icon' => 'laptop-outlined',
                'order' => 101,
                'common' => 0,
                'created_at' => NULL,
            ),
            8 => 
            array (
                'admin_route_id' => 50,
                'name' => 'DevAdminRoute',
                'parent_id' => 49,
                'title' => '菜单管理',
                'show' => 1,
                'target' => NULL,
                'blank' => NULL,
                'path' => '/dev/admin-route',
                'src' => NULL,
                'component' => '/dev/adminroute/index.vue',
                'redirect' => NULL,
                'icon' => NULL,
                'order' => 10,
                'common' => 0,
                'created_at' => NULL,
            ),
            9 => 
            array (
                'admin_route_id' => 51,
                'name' => 'SettingConfig',
                'parent_id' => 5,
                'title' => '通用配置',
                'show' => 1,
                'target' => NULL,
                'blank' => NULL,
                'path' => '/setting/config',
                'src' => NULL,
                'component' => '/setting/config/index.vue',
                'redirect' => NULL,
                'icon' => NULL,
                'order' => 0,
                'common' => 0,
                'created_at' => '2023-09-19 21:56:07',
            ),
            10 => 
            array (
                'admin_route_id' => 52,
                'name' => 'DevRedis',
                'parent_id' => 49,
                'title' => 'Redis管理',
                'show' => 1,
                'target' => NULL,
                'blank' => NULL,
                'path' => '/dev/redis',
                'src' => NULL,
                'component' => '/dev/redis/index.vue',
                'redirect' => NULL,
                'icon' => NULL,
                'order' => 0,
                'common' => 0,
                'created_at' => '2023-09-23 09:43:05',
            ),
            11 => 
            array (
                'admin_route_id' => 53,
                'name' => 'Log',
                'parent_id' => 5,
                'title' => '操作日志',
                'show' => 1,
                'target' => NULL,
                'blank' => NULL,
                'path' => '/setting/log',
                'src' => NULL,
                'component' => '/setting/log/index.vue',
                'redirect' => NULL,
                'icon' => NULL,
                'order' => 0,
                'common' => 0,
                'created_at' => '2023-09-23 18:26:21',
            ),
        ));
        
        
    }
}