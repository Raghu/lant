<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AdminNoticeTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('admin_notice')->delete();
        
        \DB::table('admin_notice')->insert(array (
            0 => 
            array (
                'admin_notice_id' => 1,
                'title' => '待办提醒',
                'content' => '123',
                'send_admin_id' => 3,
                'admin_id' => 1,
                'is_read' => 1,
                'created_at' => '2023-09-24 17:21:55',
            ),
            1 => 
            array (
                'admin_notice_id' => 2,
                'title' => '待办提醒',
                'content' => '123',
                'send_admin_id' => 3,
                'admin_id' => 1,
                'is_read' => 1,
                'created_at' => '2023-09-24 17:27:40',
            ),
            2 => 
            array (
                'admin_notice_id' => 3,
                'title' => '待办提醒',
                'content' => '123',
                'send_admin_id' => 3,
                'admin_id' => 1,
                'is_read' => 1,
                'created_at' => '2023-09-24 17:28:06',
            ),
            3 => 
            array (
                'admin_notice_id' => 4,
                'title' => '待办提醒',
                'content' => '123',
                'send_admin_id' => 3,
                'admin_id' => 1,
                'is_read' => 1,
                'created_at' => '2023-09-24 17:30:19',
            ),
            4 => 
            array (
                'admin_notice_id' => 5,
                'title' => '待办提醒',
                'content' => '123',
                'send_admin_id' => 3,
                'admin_id' => 1,
                'is_read' => 1,
                'created_at' => '2023-09-24 17:30:20',
            ),
            5 => 
            array (
                'admin_notice_id' => 6,
                'title' => '待办提醒',
                'content' => '123',
                'send_admin_id' => 3,
                'admin_id' => 1,
                'is_read' => 0,
                'created_at' => '2023-09-24 17:30:21',
            ),
            6 => 
            array (
                'admin_notice_id' => 7,
                'title' => '待办提醒',
                'content' => '123',
                'send_admin_id' => 3,
                'admin_id' => 1,
                'is_read' => 0,
                'created_at' => '2023-09-24 17:30:22',
            ),
            7 => 
            array (
                'admin_notice_id' => 8,
                'title' => '待办提醒',
                'content' => '123',
                'send_admin_id' => 3,
                'admin_id' => 1,
                'is_read' => 0,
                'created_at' => '2023-09-24 17:30:23',
            ),
            8 => 
            array (
                'admin_notice_id' => 9,
                'title' => '待办提醒',
                'content' => '123',
                'send_admin_id' => 3,
                'admin_id' => 1,
                'is_read' => 0,
                'created_at' => '2023-09-24 17:30:23',
            ),
            9 => 
            array (
                'admin_notice_id' => 10,
                'title' => '待办提醒',
                'content' => '123',
                'send_admin_id' => 3,
                'admin_id' => 1,
                'is_read' => 0,
                'created_at' => '2023-09-24 17:30:24',
            ),
            10 => 
            array (
                'admin_notice_id' => 11,
                'title' => '待办提醒',
                'content' => '123',
                'send_admin_id' => 3,
                'admin_id' => 1,
                'is_read' => 0,
                'created_at' => '2023-09-24 17:30:24',
            ),
            11 => 
            array (
                'admin_notice_id' => 12,
                'title' => '待办提醒',
                'content' => '123',
                'send_admin_id' => 3,
                'admin_id' => 1,
                'is_read' => 0,
                'created_at' => '2023-09-24 17:30:25',
            ),
            12 => 
            array (
                'admin_notice_id' => 13,
                'title' => '待办提醒',
                'content' => '123',
                'send_admin_id' => 3,
                'admin_id' => 1,
                'is_read' => 1,
                'created_at' => '2023-09-24 18:07:25',
            ),
            13 => 
            array (
                'admin_notice_id' => 14,
                'title' => '待办提醒',
                'content' => '123',
                'send_admin_id' => 3,
                'admin_id' => 1,
                'is_read' => 1,
                'created_at' => '2023-09-24 18:07:28',
            ),
            14 => 
            array (
                'admin_notice_id' => 15,
                'title' => '待办提醒',
                'content' => '123',
                'send_admin_id' => 3,
                'admin_id' => 1,
                'is_read' => 1,
                'created_at' => '2023-09-24 18:07:29',
            ),
            15 => 
            array (
                'admin_notice_id' => 16,
                'title' => '待办提醒',
                'content' => '123',
                'send_admin_id' => 3,
                'admin_id' => 1,
                'is_read' => 1,
                'created_at' => '2023-09-24 18:07:31',
            ),
            16 => 
            array (
                'admin_notice_id' => 17,
                'title' => '待办提醒',
                'content' => '123',
                'send_admin_id' => 3,
                'admin_id' => 1,
                'is_read' => 1,
                'created_at' => '2023-09-24 18:07:31',
            ),
            17 => 
            array (
                'admin_notice_id' => 18,
                'title' => '待办提醒',
                'content' => '123',
                'send_admin_id' => 3,
                'admin_id' => 1,
                'is_read' => 1,
                'created_at' => '2023-09-24 18:07:32',
            ),
        ));
        
        
    }
}