<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class FileTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('file')->delete();
        
        \DB::table('file')->insert(array (
            0 => 
            array (
                'file_id' => 1,
                'dir' => 'admin',
                'path' => 'admin/J3nVdwhisQgdi6dWmUokUGHt4xAy8OGDjVZEQxpM.mp4',
                'filename' => 'WeChat_20230917110201.mp4',
                'file_extension' => 'mp4',
                'mime_type' => 'video/mp4',
                'size' => 9556329,
                'access' => 'public',
                'code' => 'eeb48563f27e0fac9ac9b05b9e306146',
                'created_at' => '2023-09-17 11:14:11',
            ),
            1 => 
            array (
                'file_id' => 2,
                'dir' => 'admin',
                'path' => 'admin/9FOg6AbdDUGyw4D0hiDOoODZABY1So7tmZb5wlwx.mp4',
                'filename' => 'WeChat_20230917110201.mp4',
                'file_extension' => 'mp4',
                'mime_type' => 'video/mp4',
                'size' => 9556329,
                'access' => 'public',
                'code' => 'dc63c500d7be03f577a80655fadcb2f1',
                'created_at' => '2023-09-17 11:19:49',
            ),
            2 => 
            array (
                'file_id' => 3,
                'dir' => 'admin',
                'path' => 'admin/oJSrUsoO3c8jAoV4yM24yJDNqeqRGAJcAAnhXmMj.mp4',
                'filename' => 'WeChat_20230917110201.mp4',
                'file_extension' => 'mp4',
                'mime_type' => 'video/mp4',
                'size' => 9556329,
                'access' => 'public',
                'code' => '1c9aa43129ab950994d0e1378de9dbc5',
                'created_at' => '2023-09-17 11:21:49',
            ),
            3 => 
            array (
                'file_id' => 4,
                'dir' => 'admin',
                'path' => 'admin/2feIFl54R6PXHCJJZMpt1uVG9MEiUV3QvqeVkPmb.mp4',
                'filename' => 'WeChat_20230917110201.mp4',
                'file_extension' => 'mp4',
                'mime_type' => 'video/mp4',
                'size' => 9556329,
                'access' => 'public',
                'code' => '8b17efaeee0f03eef4e99509c887b204',
                'created_at' => '2023-09-17 11:22:00',
            ),
            4 => 
            array (
                'file_id' => 5,
                'dir' => 'admin',
                'path' => 'admin/2ce2nLfINaEtoSwcWE9lywwfzgw5uW6GLl6zTXY7.mp4',
                'filename' => 'WeChat_20230917110201.mp4',
                'file_extension' => 'mp4',
                'mime_type' => 'video/mp4',
                'size' => 9556329,
                'access' => 'public',
                'code' => '3a0a7b12867c0d9b9c4fdb6f34354577',
                'created_at' => '2023-09-17 11:23:20',
            ),
            5 => 
            array (
                'file_id' => 6,
                'dir' => 'admin',
                'path' => 'admin/0zV4ZbRB3IoymSjhIIF7PpKvKQ21sfEHK8VBUI6i.mp4',
                'filename' => 'WeChat_20230917110201.mp4',
                'file_extension' => 'mp4',
                'mime_type' => 'video/mp4',
                'size' => 9556329,
                'access' => 'public',
                'code' => 'ef3af0bbe9a150ecee7631254259016e',
                'created_at' => '2023-09-17 11:23:41',
            ),
            6 => 
            array (
                'file_id' => 7,
                'dir' => 'admin',
                'path' => 'admin/jr2xSMpW2egF1OtdQSkBdKKJxd03VmBP9hmVd8Pp.mp4',
                'filename' => 'WeChat_20230917110201.mp4',
                'file_extension' => 'mp4',
                'mime_type' => 'video/mp4',
                'size' => 9556329,
                'access' => 'public',
                'code' => 'd6a6e9350795dc7f4ba675e1484f4e4e',
                'created_at' => '2023-09-17 11:23:48',
            ),
            7 => 
            array (
                'file_id' => 8,
                'dir' => 'admin',
                'path' => 'admin/FTUKveLxgmVCff4x3zMvXKdcEF8zRau3Op7k6mqL.mp4',
                'filename' => 'WeChat_20230917110201.mp4',
                'file_extension' => 'mp4',
                'mime_type' => 'video/mp4',
                'size' => 9556329,
                'access' => 'public',
                'code' => '69381d9569fcf43dde81dcad59655102',
                'created_at' => '2023-09-17 11:24:09',
            ),
            8 => 
            array (
                'file_id' => 9,
                'dir' => 'admin',
                'path' => 'admin/GJ4pMSAooFSUBSBR1FeEReglFeSCjJxrwKV66rDR.mp4',
                'filename' => 'WeChat_20230917110201.mp4',
                'file_extension' => 'mp4',
                'mime_type' => 'video/mp4',
                'size' => 9556329,
                'access' => 'public',
                'code' => '70841e8bbf7e34be71614b523a6680cb',
                'created_at' => '2023-09-17 11:35:32',
            ),
            9 => 
            array (
                'file_id' => 10,
                'dir' => 'admin',
                'path' => 'admin/W5uI6UpyjSWLFFjPHPFbEMIQYCzOsyOTKUropHtM.mp4',
                'filename' => 'WeChat_20230920205208.mp4',
                'file_extension' => 'mp4',
                'mime_type' => 'video/mp4',
                'size' => 2394109,
                'access' => 'public',
                'code' => '3a2b85994d4b6e2b9df4f7b4b73ad6c4',
                'created_at' => '2023-09-20 20:52:14',
            ),
            10 => 
            array (
                'file_id' => 11,
                'dir' => 'admin',
                'path' => 'admin/iPLaYp4SBR46fbxq5HRLElcbuFBX5v1K2v0dbMi6.mp4',
                'filename' => 'WeChat_20230920205208.mp4',
                'file_extension' => 'mp4',
                'mime_type' => 'video/mp4',
                'size' => 2394109,
                'access' => 'public',
                'code' => '948293957711f7181eb29bddf085c9c9',
                'created_at' => '2023-09-20 21:07:08',
            ),
            11 => 
            array (
                'file_id' => 12,
                'dir' => 'admin',
                'path' => 'admin/waTL3ekBAad7prHnQc5kONLadr87JypAgZUrI4Ht.mp4',
                'filename' => 'WeChat_20230920205208.mp4',
                'file_extension' => 'mp4',
                'mime_type' => 'video/mp4',
                'size' => 2394109,
                'access' => 'public',
                'code' => '89c243833fd8611f97adfab01cca149a',
                'created_at' => '2023-09-20 21:08:48',
            ),
            12 => 
            array (
                'file_id' => 13,
                'dir' => 'admin',
                'path' => 'admin/B02sAMrzoSgR2EEySI9yQ7YeZz3VBQahQG0T3IpV.mp4',
                'filename' => 'WeChat_20230920205208.mp4',
                'file_extension' => 'mp4',
                'mime_type' => 'video/mp4',
                'size' => 2394109,
                'access' => 'public',
                'code' => '7770b516e1eca571968bc913e6b3d66e',
                'created_at' => '2023-09-20 21:09:39',
            ),
            13 => 
            array (
                'file_id' => 14,
                'dir' => 'admin',
                'path' => 'admin/WgHIqmCTFUrcOxO1jJOMBkpv9f1G1gNmeyNSUuVO.mp4',
                'filename' => 'WeChat_20230920205208.mp4',
                'file_extension' => 'mp4',
                'mime_type' => 'video/mp4',
                'size' => 2394109,
                'access' => 'public',
                'code' => '1ddf2cce897f16ec29f75aa05ff697c3',
                'created_at' => '2023-09-20 21:14:10',
            ),
            14 => 
            array (
                'file_id' => 15,
                'dir' => 'admin',
                'path' => 'admin/OavmGAbZREfJt6JkCdT13KTkaY1JZFDWxtOqDpsZ.mp4',
                'filename' => 'WeChat_20230920205208.mp4',
                'file_extension' => 'mp4',
                'mime_type' => 'video/mp4',
                'size' => 2394109,
                'access' => 'public',
                'code' => 'fe1ff547ca4d5060bdd47d35fe69068b',
                'created_at' => '2023-09-20 21:14:36',
            ),
            15 => 
            array (
                'file_id' => 16,
                'dir' => 'admin',
                'path' => 'admin/nREUQWYN3Y5dIGGveO3OX6hNFS2W91eDmY5yTdTu.mp4',
                'filename' => 'WeChat_20230920205208.mp4',
                'file_extension' => 'mp4',
                'mime_type' => 'video/mp4',
                'size' => 2394109,
                'access' => 'public',
                'code' => '9a07d4ae60f56098dfb746dd04a46ceb',
                'created_at' => '2023-09-20 21:15:03',
            ),
        ));
        
        
    }
}