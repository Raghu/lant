<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ActivityLogTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('activity_log')->delete();
        
        \DB::table('activity_log')->insert(array (
            0 => 
            array (
                'id' => 1,
                'log_name' => 'admin',
                'description' => 'updated',
                'subject_type' => 'App\\Models\\Admin',
                'subject_id' => 6,
                'causer_type' => 'App\\Models\\Admin',
                'causer_id' => 1,
                'admin_operation_log_id' => 1,
                'properties' => '{"old": {"name": "1231231"}, "attributes": {"name": "12312315"}}',
                'created_at' => '2023-09-23 21:16:43',
                'updated_at' => '2023-09-23 21:16:43',
            ),
            1 => 
            array (
                'id' => 2,
                'log_name' => 'admin',
                'description' => 'updated',
                'subject_type' => 'App\\Models\\Admin',
                'subject_id' => 3,
                'causer_type' => 'App\\Models\\Admin',
                'causer_id' => 1,
                'admin_operation_log_id' => 2,
                'properties' => '{"old": {"avatar_image_id": null}, "attributes": {"avatar_image_id": 251}}',
                'created_at' => '2023-09-23 22:40:33',
                'updated_at' => '2023-09-23 22:40:33',
            ),
            2 => 
            array (
                'id' => 3,
                'log_name' => 'admin',
                'description' => 'updated',
                'subject_type' => 'App\\Models\\Admin',
                'subject_id' => 3,
                'causer_type' => NULL,
                'causer_id' => NULL,
                'admin_operation_log_id' => NULL,
                'properties' => '{"old": [], "attributes": []}',
                'created_at' => '2023-09-23 22:40:37',
                'updated_at' => '2023-09-23 22:40:37',
            ),
            3 => 
            array (
                'id' => 4,
                'log_name' => 'admin',
                'description' => 'updated',
                'subject_type' => 'App\\Models\\Admin',
                'subject_id' => 3,
                'causer_type' => NULL,
                'causer_id' => NULL,
                'admin_operation_log_id' => NULL,
                'properties' => '{"old": [], "attributes": []}',
                'created_at' => '2023-09-24 12:28:04',
                'updated_at' => '2023-09-24 12:28:04',
            ),
            4 => 
            array (
                'id' => 5,
                'log_name' => 'admin',
                'description' => 'updated',
                'subject_type' => 'App\\Models\\Admin',
                'subject_id' => 3,
                'causer_type' => NULL,
                'causer_id' => NULL,
                'admin_operation_log_id' => NULL,
                'properties' => '{"old": [], "attributes": []}',
                'created_at' => '2023-09-24 16:18:58',
                'updated_at' => '2023-09-24 16:18:58',
            ),
            5 => 
            array (
                'id' => 6,
                'log_name' => 'admin',
                'description' => 'updated',
                'subject_type' => 'App\\Models\\Admin',
                'subject_id' => 1,
                'causer_type' => NULL,
                'causer_id' => NULL,
                'admin_operation_log_id' => NULL,
                'properties' => '{"old": [], "attributes": []}',
                'created_at' => '2023-09-24 16:20:29',
                'updated_at' => '2023-09-24 16:20:29',
            ),
            6 => 
            array (
                'id' => 7,
                'log_name' => 'admin',
                'description' => 'updated',
                'subject_type' => 'App\\Models\\Admin',
                'subject_id' => 3,
                'causer_type' => NULL,
                'causer_id' => NULL,
                'admin_operation_log_id' => NULL,
                'properties' => '{"old": [], "attributes": []}',
                'created_at' => '2023-09-24 16:23:08',
                'updated_at' => '2023-09-24 16:23:08',
            ),
            7 => 
            array (
                'id' => 8,
                'log_name' => 'admin',
                'description' => 'updated',
                'subject_type' => 'App\\Models\\Admin',
                'subject_id' => 1,
                'causer_type' => NULL,
                'causer_id' => NULL,
                'admin_operation_log_id' => NULL,
                'properties' => '{"old": [], "attributes": []}',
                'created_at' => '2023-09-24 16:24:20',
                'updated_at' => '2023-09-24 16:24:20',
            ),
            8 => 
            array (
                'id' => 9,
                'log_name' => 'admin',
                'description' => 'updated',
                'subject_type' => 'App\\Models\\Admin',
                'subject_id' => 6,
                'causer_type' => 'App\\Models\\Admin',
                'causer_id' => 3,
                'admin_operation_log_id' => 4,
                'properties' => '{"old": {"name": "12312315"}, "attributes": {"name": "123123152"}}',
                'created_at' => '2023-09-24 16:28:25',
                'updated_at' => '2023-09-24 16:28:25',
            ),
            9 => 
            array (
                'id' => 10,
                'log_name' => 'admin',
                'description' => 'updated',
                'subject_type' => 'App\\Models\\Admin',
                'subject_id' => 6,
                'causer_type' => 'App\\Models\\Admin',
                'causer_id' => 3,
                'admin_operation_log_id' => 5,
                'properties' => '{"old": {"name": "123123152"}, "attributes": {"name": "1231231522"}}',
                'created_at' => '2023-09-24 16:28:27',
                'updated_at' => '2023-09-24 16:28:27',
            ),
            10 => 
            array (
                'id' => 11,
                'log_name' => 'admin',
                'description' => 'updated',
                'subject_type' => 'App\\Models\\Admin',
                'subject_id' => 6,
                'causer_type' => 'App\\Models\\Admin',
                'causer_id' => 3,
                'admin_operation_log_id' => 6,
                'properties' => '{"old": {"name": "1231231522"}, "attributes": {"name": "12312315222"}}',
                'created_at' => '2023-09-24 16:28:29',
                'updated_at' => '2023-09-24 16:28:29',
            ),
            11 => 
            array (
                'id' => 12,
                'log_name' => 'admin',
                'description' => 'updated',
                'subject_type' => 'App\\Models\\Admin',
                'subject_id' => 6,
                'causer_type' => 'App\\Models\\Admin',
                'causer_id' => 3,
                'admin_operation_log_id' => 7,
                'properties' => '{"old": {"name": "12312315222"}, "attributes": {"name": "123123152222"}}',
                'created_at' => '2023-09-24 16:28:31',
                'updated_at' => '2023-09-24 16:28:31',
            ),
            12 => 
            array (
                'id' => 13,
                'log_name' => 'admin',
                'description' => 'updated',
                'subject_type' => 'App\\Models\\Admin',
                'subject_id' => 6,
                'causer_type' => 'App\\Models\\Admin',
                'causer_id' => 1,
                'admin_operation_log_id' => 8,
                'properties' => '{"old": {"name": "123123152222"}, "attributes": {"name": "1231231522222"}}',
                'created_at' => '2023-09-24 16:28:56',
                'updated_at' => '2023-09-24 16:28:56',
            ),
            13 => 
            array (
                'id' => 14,
                'log_name' => 'admin',
                'description' => 'updated',
                'subject_type' => 'App\\Models\\Admin',
                'subject_id' => 6,
                'causer_type' => 'App\\Models\\Admin',
                'causer_id' => 1,
                'admin_operation_log_id' => 9,
                'properties' => '{"old": {"name": "1231231522222"}, "attributes": {"name": "12312315222222"}}',
                'created_at' => '2023-09-24 16:28:59',
                'updated_at' => '2023-09-24 16:28:59',
            ),
            14 => 
            array (
                'id' => 15,
                'log_name' => 'admin',
                'description' => 'updated',
                'subject_type' => 'App\\Models\\Admin',
                'subject_id' => 6,
                'causer_type' => 'App\\Models\\Admin',
                'causer_id' => 1,
                'admin_operation_log_id' => 10,
                'properties' => '{"old": {"name": "12312315222222"}, "attributes": {"name": "123123152222222"}}',
                'created_at' => '2023-09-24 16:29:01',
                'updated_at' => '2023-09-24 16:29:01',
            ),
            15 => 
            array (
                'id' => 16,
                'log_name' => 'admin',
                'description' => 'updated',
                'subject_type' => 'App\\Models\\Admin',
                'subject_id' => 6,
                'causer_type' => 'App\\Models\\Admin',
                'causer_id' => 1,
                'admin_operation_log_id' => 11,
                'properties' => '{"old": {"name": "123123152222222"}, "attributes": {"name": "1231231522222222"}}',
                'created_at' => '2023-09-24 16:29:03',
                'updated_at' => '2023-09-24 16:29:03',
            ),
            16 => 
            array (
                'id' => 17,
                'log_name' => 'admin',
                'description' => 'updated',
                'subject_type' => 'App\\Models\\Admin',
                'subject_id' => 3,
                'causer_type' => NULL,
                'causer_id' => NULL,
                'admin_operation_log_id' => NULL,
                'properties' => '{"old": [], "attributes": []}',
                'created_at' => '2023-09-24 16:38:58',
                'updated_at' => '2023-09-24 16:38:58',
            ),
            17 => 
            array (
                'id' => 18,
                'log_name' => 'admin',
                'description' => 'updated',
                'subject_type' => 'App\\Models\\Admin',
                'subject_id' => 1,
                'causer_type' => NULL,
                'causer_id' => NULL,
                'admin_operation_log_id' => NULL,
                'properties' => '{"old": [], "attributes": []}',
                'created_at' => '2023-09-24 16:40:27',
                'updated_at' => '2023-09-24 16:40:27',
            ),
            18 => 
            array (
                'id' => 19,
                'log_name' => 'admin',
                'description' => 'updated',
                'subject_type' => 'App\\Models\\Admin',
                'subject_id' => 3,
                'causer_type' => NULL,
                'causer_id' => NULL,
                'admin_operation_log_id' => NULL,
                'properties' => '{"old": [], "attributes": []}',
                'created_at' => '2023-09-27 21:12:46',
                'updated_at' => '2023-09-27 21:12:46',
            ),
            19 => 
            array (
                'id' => 20,
                'log_name' => 'admin',
                'description' => 'updated',
                'subject_type' => 'App\\Models\\Admin',
                'subject_id' => 1,
                'causer_type' => NULL,
                'causer_id' => NULL,
                'admin_operation_log_id' => NULL,
                'properties' => '{"old": [], "attributes": []}',
                'created_at' => '2023-09-28 20:49:09',
                'updated_at' => '2023-09-28 20:49:09',
            ),
            20 => 
            array (
                'id' => 21,
                'log_name' => 'admin',
                'description' => 'updated',
                'subject_type' => 'App\\Models\\Admin',
                'subject_id' => 1,
                'causer_type' => NULL,
                'causer_id' => NULL,
                'admin_operation_log_id' => NULL,
                'properties' => '{"old": [], "attributes": []}',
                'created_at' => '2023-11-05 12:41:05',
                'updated_at' => '2023-11-05 12:41:05',
            ),
            21 => 
            array (
                'id' => 22,
                'log_name' => 'admin',
                'description' => 'updated',
                'subject_type' => 'App\\Models\\Admin',
                'subject_id' => 1,
                'causer_type' => NULL,
                'causer_id' => NULL,
                'admin_operation_log_id' => NULL,
                'properties' => '{"old": [], "attributes": []}',
                'created_at' => '2024-02-08 13:56:18',
                'updated_at' => '2024-02-08 13:56:18',
            ),
            22 => 
            array (
                'id' => 23,
                'log_name' => 'admin',
                'description' => 'deleted',
                'subject_type' => 'App\\Models\\Admin',
                'subject_id' => 1,
                'causer_type' => 'App\\Models\\Admin',
                'causer_id' => 1,
                'admin_operation_log_id' => 21,
                'properties' => '{"attributes": {"name": "超级管理员", "status": 1, "username": "admin", "admin_role_id": null, "avatar_image_id": 237}}',
                'created_at' => '2024-02-08 15:36:38',
                'updated_at' => '2024-02-08 15:36:38',
            ),
            23 => 
            array (
                'id' => 24,
                'log_name' => 'admin',
                'description' => 'updated',
                'subject_type' => 'App\\Models\\Admin',
                'subject_id' => 1,
                'causer_type' => NULL,
                'causer_id' => NULL,
                'admin_operation_log_id' => NULL,
                'properties' => '{"old": [], "attributes": []}',
                'created_at' => '2024-02-08 15:38:22',
                'updated_at' => '2024-02-08 15:38:22',
            ),
            24 => 
            array (
                'id' => 25,
                'log_name' => 'admin',
                'description' => 'updated',
                'subject_type' => 'App\\Models\\Admin',
                'subject_id' => 3,
                'causer_type' => NULL,
                'causer_id' => NULL,
                'admin_operation_log_id' => NULL,
                'properties' => '{"old": [], "attributes": []}',
                'created_at' => '2024-02-08 15:40:58',
                'updated_at' => '2024-02-08 15:40:58',
            ),
        ));
        
        
    }
}