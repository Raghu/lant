<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AdminOperationLogTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('admin_operation_log')->delete();
        
        \DB::table('admin_operation_log')->insert(array (
            0 => 
            array (
                'admin_operation_log_id' => 1,
                'admin_id' => 1,
                'title' => '编辑管理员',
                'action' => 'edit',
                'created_at' => '2023-09-23 21:16:43',
                'ip' => '192.168.33.1',
                'domain_id' => NULL,
            ),
            1 => 
            array (
                'admin_operation_log_id' => 2,
                'admin_id' => 1,
                'title' => '编辑管理员',
                'action' => 'edit',
                'created_at' => '2023-09-23 22:40:33',
                'ip' => '192.168.33.1',
                'domain_id' => NULL,
            ),
            2 => 
            array (
                'admin_operation_log_id' => 3,
                'admin_id' => 1,
                'title' => '编辑管理员',
                'action' => 'edit',
                'created_at' => '2023-09-24 09:53:10',
                'ip' => '192.168.33.1',
                'domain_id' => NULL,
            ),
            3 => 
            array (
                'admin_operation_log_id' => 4,
                'admin_id' => 3,
                'title' => '编辑管理员',
                'action' => 'edit',
                'created_at' => '2023-09-24 16:28:25',
                'ip' => '192.168.33.1',
                'domain_id' => NULL,
            ),
            4 => 
            array (
                'admin_operation_log_id' => 5,
                'admin_id' => 3,
                'title' => '编辑管理员',
                'action' => 'edit',
                'created_at' => '2023-09-24 16:28:27',
                'ip' => '192.168.33.1',
                'domain_id' => NULL,
            ),
            5 => 
            array (
                'admin_operation_log_id' => 6,
                'admin_id' => 3,
                'title' => '编辑管理员',
                'action' => 'edit',
                'created_at' => '2023-09-24 16:28:29',
                'ip' => '192.168.33.1',
                'domain_id' => NULL,
            ),
            6 => 
            array (
                'admin_operation_log_id' => 7,
                'admin_id' => 3,
                'title' => '编辑管理员',
                'action' => 'edit',
                'created_at' => '2023-09-24 16:28:31',
                'ip' => '192.168.33.1',
                'domain_id' => NULL,
            ),
            7 => 
            array (
                'admin_operation_log_id' => 8,
                'admin_id' => 1,
                'title' => '编辑管理员',
                'action' => 'edit',
                'created_at' => '2023-09-24 16:28:56',
                'ip' => '192.168.33.1',
                'domain_id' => NULL,
            ),
            8 => 
            array (
                'admin_operation_log_id' => 9,
                'admin_id' => 1,
                'title' => '编辑管理员',
                'action' => 'edit',
                'created_at' => '2023-09-24 16:28:59',
                'ip' => '192.168.33.1',
                'domain_id' => NULL,
            ),
            9 => 
            array (
                'admin_operation_log_id' => 10,
                'admin_id' => 1,
                'title' => '编辑管理员',
                'action' => 'edit',
                'created_at' => '2023-09-24 16:29:01',
                'ip' => '192.168.33.1',
                'domain_id' => NULL,
            ),
            10 => 
            array (
                'admin_operation_log_id' => 11,
                'admin_id' => 1,
                'title' => '编辑管理员',
                'action' => 'edit',
                'created_at' => '2023-09-24 16:29:03',
                'ip' => '192.168.33.1',
                'domain_id' => NULL,
            ),
            11 => 
            array (
                'admin_operation_log_id' => 12,
                'admin_id' => 1,
                'title' => '编辑管理员',
                'action' => 'edit',
                'created_at' => '2023-11-05 12:41:30',
                'ip' => '192.168.33.1',
                'domain_id' => NULL,
            ),
            12 => 
            array (
                'admin_operation_log_id' => 13,
                'admin_id' => 1,
                'title' => '编辑管理员',
                'action' => 'edit',
                'created_at' => '2023-11-05 12:41:35',
                'ip' => '192.168.33.1',
                'domain_id' => NULL,
            ),
            13 => 
            array (
                'admin_operation_log_id' => 14,
                'admin_id' => 1,
                'title' => '编辑管理员',
                'action' => 'edit',
                'created_at' => '2023-11-05 12:41:45',
                'ip' => '192.168.33.1',
                'domain_id' => NULL,
            ),
            14 => 
            array (
                'admin_operation_log_id' => 15,
                'admin_id' => 1,
                'title' => '编辑管理员',
                'action' => 'edit',
                'created_at' => '2023-11-05 12:41:51',
                'ip' => '192.168.33.1',
                'domain_id' => NULL,
            ),
            15 => 
            array (
                'admin_operation_log_id' => 16,
                'admin_id' => 1,
                'title' => '编辑管理员',
                'action' => 'edit',
                'created_at' => '2023-11-05 12:44:59',
                'ip' => '192.168.33.1',
                'domain_id' => NULL,
            ),
            16 => 
            array (
                'admin_operation_log_id' => 17,
                'admin_id' => 1,
                'title' => '编辑管理员',
                'action' => 'edit',
                'created_at' => '2023-11-05 12:45:27',
                'ip' => '192.168.33.1',
                'domain_id' => NULL,
            ),
            17 => 
            array (
                'admin_operation_log_id' => 18,
                'admin_id' => 1,
                'title' => '编辑管理员',
                'action' => 'edit',
                'created_at' => '2023-11-05 12:51:53',
                'ip' => '192.168.33.1',
                'domain_id' => NULL,
            ),
            18 => 
            array (
                'admin_operation_log_id' => 19,
                'admin_id' => 1,
                'title' => '编辑管理员',
                'action' => 'edit',
                'created_at' => '2023-11-05 12:52:49',
                'ip' => '192.168.33.1',
                'domain_id' => NULL,
            ),
            19 => 
            array (
                'admin_operation_log_id' => 20,
                'admin_id' => 1,
                'title' => '编辑管理员',
                'action' => 'edit',
                'created_at' => '2023-11-05 12:52:55',
                'ip' => '192.168.33.1',
                'domain_id' => NULL,
            ),
            20 => 
            array (
                'admin_operation_log_id' => 21,
                'admin_id' => 1,
                'title' => '删除管理员',
                'action' => 'remove',
                'created_at' => '2024-02-08 15:36:38',
                'ip' => '192.168.33.1',
                'domain_id' => NULL,
            ),
            21 => 
            array (
                'admin_operation_log_id' => 22,
                'admin_id' => 3,
                'title' => '编辑配置项',
                'action' => 'edit',
                'created_at' => '2024-02-08 18:18:27',
                'ip' => '192.168.33.1',
                'domain_id' => NULL,
            ),
            22 => 
            array (
                'admin_operation_log_id' => 23,
                'admin_id' => 3,
                'title' => '编辑配置项',
                'action' => 'edit',
                'created_at' => '2024-02-08 18:18:29',
                'ip' => '192.168.33.1',
                'domain_id' => NULL,
            ),
        ));
        
        
    }
}