<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permissions')->delete();
        
        \DB::table('permissions')->insert(array (
            0 => 
            array (
                'id' => 2,
                'name' => 'admin.add',
                'guard_name' => 'admin',
                'module' => 'Setting',
                'control' => 'Admin',
                'desc' => '添加管理员',
                'created_at' => '2023-05-13 03:02:47',
                'updated_at' => '2023-05-13 03:02:47',
            ),
            1 => 
            array (
                'id' => 3,
                'name' => 'admin.edit',
                'guard_name' => 'admin',
                'module' => 'Setting',
                'control' => 'Admin',
                'desc' => '编辑管理员',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 4,
                'name' => 'admin.browse',
                'guard_name' => 'admin',
                'module' => 'Setting',
                'control' => 'Admin',
                'desc' => '浏览列表',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 5,
                'name' => 'admin.remove',
                'guard_name' => 'admin',
                'module' => 'Setting',
                'control' => 'Admin',
                'desc' => '删除管理员',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 6,
                'name' => 'adminrole.add',
                'guard_name' => 'admin',
                'module' => 'Setting',
                'control' => 'AdminRole',
                'desc' => '添加角色',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 7,
                'name' => 'adminrole.edit',
                'guard_name' => 'admin',
                'module' => 'Setting',
                'control' => 'AdminRole',
                'desc' => '编辑角色',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 8,
                'name' => 'adminrole.browse',
                'guard_name' => 'admin',
                'module' => 'Setting',
                'control' => 'AdminRole',
                'desc' => '浏览列表',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 9,
                'name' => 'adminrole.remove',
                'guard_name' => 'admin',
                'module' => 'Setting',
                'control' => 'AdminRole',
                'desc' => '删除角色',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 10,
                'name' => 'adminrole.setpermission',
                'guard_name' => 'admin',
                'module' => 'Setting',
                'control' => 'AdminRole',
                'desc' => '设置权限',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 108,
                'name' => 'adminroute.add',
                'guard_name' => 'admin',
                'module' => 'Dev',
                'control' => 'DevAdminRoute',
                'desc' => '添加菜单',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 109,
                'name' => 'adminroute.edit',
                'guard_name' => 'admin',
                'module' => 'Dev',
                'control' => 'DevAdminRoute',
                'desc' => '编辑菜单',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 110,
                'name' => 'adminroute.browse',
                'guard_name' => 'admin',
                'module' => 'Dev',
                'control' => 'DevAdminRoute',
                'desc' => '浏览菜单',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 111,
                'name' => 'adminroute.remove',
                'guard_name' => 'admin',
                'module' => 'Dev',
                'control' => 'DevAdminRoute',
                'desc' => '删除菜单',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 112,
                'name' => 'adminroute.setpermission',
                'guard_name' => 'admin',
                'module' => 'Dev',
                'control' => 'DevAdminRoute',
                'desc' => '方法管理',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => 118,
                'name' => 'config.browse',
                'guard_name' => 'admin',
                'module' => 'Setting',
                'control' => 'SettingConfig',
                'desc' => '查看配置',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => 119,
                'name' => 'config.edit',
                'guard_name' => 'admin',
                'module' => 'Setting',
                'control' => 'SettingConfig',
                'desc' => '修改配置',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => 120,
                'name' => 'redis.browse',
                'guard_name' => 'admin',
                'module' => 'DevRedis',
                'control' => 'DevRedis',
                'desc' => '浏览缓存',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => 121,
                'name' => 'redis.edit',
                'guard_name' => 'admin',
                'module' => 'DevRedis',
                'control' => 'DevRedis',
                'desc' => '修改缓存值',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => 122,
                'name' => 'redis.ttl',
                'guard_name' => 'admin',
                'module' => 'DevRedis',
                'control' => 'DevRedis',
                'desc' => '设置过期时间',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => 123,
                'name' => 'redis.remove',
                'guard_name' => 'admin',
                'module' => 'DevRedis',
                'control' => 'DevRedis',
                'desc' => '删除键',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => 124,
                'name' => 'log.browse',
                'guard_name' => 'admin',
                'module' => 'Dev',
                'control' => 'Log',
                'desc' => '浏览',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}