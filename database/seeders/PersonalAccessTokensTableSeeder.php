<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PersonalAccessTokensTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('personal_access_tokens')->delete();
        
        \DB::table('personal_access_tokens')->insert(array (
            0 => 
            array (
                'id' => 154,
                'tokenable_type' => 'App\\Models\\Admin',
                'tokenable_id' => 2,
                'name' => 'admin',
                'token' => '1f8e1cdf3306cd20f966165a3cbe4011506e67ad89e87f3b0be5e5336356d31c',
                'abilities' => '["*"]',
                'last_used_at' => '2023-07-16 11:48:11',
                'created_at' => '2023-07-16 11:21:05',
                'updated_at' => '2023-07-16 11:48:11',
            ),
            1 => 
            array (
                'id' => 180,
                'tokenable_type' => 'App\\Models\\User',
                'tokenable_id' => 1,
                'name' => 'user',
                'token' => 'a97b7b734f4055b2b848ad344ce094171c56dee7c897ef94bb13ad6fdcc317ba',
                'abilities' => '["*"]',
                'last_used_at' => NULL,
                'created_at' => '2023-09-12 20:35:49',
                'updated_at' => '2023-09-12 20:35:49',
            ),
            2 => 
            array (
                'id' => 195,
                'tokenable_type' => 'App\\Models\\Admin',
                'tokenable_id' => 1,
                'name' => 'admin',
                'token' => '62ce3b65315b83b6fea6e08fef1cc3572d1fd96e134d39da33780d7b2ffe0a2d',
                'abilities' => '["*"]',
                'last_used_at' => '2024-02-08 15:40:35',
                'created_at' => '2024-02-08 15:38:22',
                'updated_at' => '2024-02-08 15:40:35',
            ),
            3 => 
            array (
                'id' => 196,
                'tokenable_type' => 'App\\Models\\Admin',
                'tokenable_id' => 3,
                'name' => 'admin',
                'token' => 'b578cf9e3a61e42d18329f88458f0be58d912c55eb332458f12def887e318519',
                'abilities' => '["*"]',
                'last_used_at' => '2024-02-08 18:36:07',
                'created_at' => '2024-02-08 15:40:58',
                'updated_at' => '2024-02-08 18:36:07',
            ),
        ));
        
        
    }
}