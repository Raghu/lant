<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('user')->delete();
        
        \DB::table('user')->insert(array (
            0 => 
            array (
                'user_id' => 1,
                'nickname' => '测试昵称',
                'mobile' => NULL,
                'password' => '',
                'status' => 0,
                'avatar_image_id' => 1,
                'sex' => 1,
                'last_login_at' => NULL,
                'last_login_ip' => NULL,
                'created_at' => NULL,
            ),
        ));
        
        
    }
}