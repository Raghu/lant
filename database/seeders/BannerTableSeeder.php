<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class BannerTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('banner')->delete();
        
        \DB::table('banner')->insert(array (
            0 => 
            array (
                'banner_id' => 18,
                'image_id' => 162,
                'location' => 1,
                'sort' => 0,
                'status' => 1,
                'created_at' => '2023-06-14 03:51:20',
                'domain_id' => 1,
            ),
            1 => 
            array (
                'banner_id' => 19,
                'image_id' => 164,
                'location' => 1,
                'sort' => 0,
                'status' => 1,
                'created_at' => '2023-07-04 17:16:13',
                'domain_id' => 4,
            ),
            2 => 
            array (
                'banner_id' => 20,
                'image_id' => 163,
                'location' => 1,
                'sort' => 0,
                'status' => 1,
                'created_at' => '2023-07-04 17:16:29',
                'domain_id' => 2,
            ),
            3 => 
            array (
                'banner_id' => 21,
                'image_id' => 128,
                'location' => 1,
                'sort' => 0,
                'status' => 1,
                'created_at' => '2023-07-04 17:16:47',
                'domain_id' => 3,
            ),
            4 => 
            array (
                'banner_id' => 22,
                'image_id' => 166,
                'location' => 1,
                'sort' => 0,
                'status' => 1,
                'created_at' => '2023-07-15 11:16:17',
                'domain_id' => 5,
            ),
        ));
        
        
    }
}