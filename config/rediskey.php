<?php
/**
 * Created by Raghu Liu
 * Date: 2023/9/23
 * Time: 10:43
 */

return [
    'smscode' => \App\Cache\SmsCodeCache::class,
    'socketid' => \App\Cache\SocketFdCache::class
];
