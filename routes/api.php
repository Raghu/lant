<?php

Route::group(['middleware' => 'api.validate:api'], function () {

    /* 不需要身份验证的路由*/
    Route::group(['prefix' => 'user'], function () {
        /* 登录 */
        Route::post('login', 'App\Http\Controllers\Api\UserController@login');
    });

    /* 需要身份验证的路由 */
    Route::group(['middleware' => ['auth:api'], 'prefix' => 'user'], function () {
        /* 示例 */
        Route::post('hellow', 'App\Http\Controllers\Api\UserController@hellow');
    });

});
