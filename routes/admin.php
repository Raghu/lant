<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api.validate:admin'], function () {

    Route::post('/auth/login', 'IndexController@login');

    Route::group(['middleware' => ['auth:admin', 'permission:common']], function () {

        /* 获取当前用户信息及权限列表 */
        Route::post('/auth/getUserInfo', 'IndexController@getUserInfo');
        /* 绑定socket id*/
        Route::post('/auth/bindSocketId', 'IndexController@bindSocketId');

        /* 后台设置 */
        Route::group(['prefix' => 'setting'], function () {

            /* 管理员设置 */
            Route::group(['prefix' => 'admin'], function () {
                /* 获取列表 */
                Route::post('browse', 'AdminController@browse');
                /* 添加 */
                Route::post('add', 'AdminController@add');
                /* 编辑 */
                Route::post('edit', 'AdminController@edit');
                /* 删除 */
                Route::post('remove', 'AdminController@remove');
            });

            /* 权限设置 */
            Route::group(['prefix' => 'adminRole'], function () {
                /* 获取列表 */
                Route::post('browse', 'AdminRoleController@browse');
                /* 添加 */
                Route::post('add', 'AdminRoleController@add');
                /* 编辑 */
                Route::post('edit', 'AdminRoleController@edit');
                /* 删除 */
                Route::post('remove', 'AdminRoleController@remove');
                /* 根据id获取当前权限 */
                Route::post('getPermission', 'AdminRoleController@getPermission');
                /* 设置权限 */
                Route::post('setPermission', 'AdminRoleController@setPermission');
            });

            /* 配置管理 */
            Route::group(['prefix' => 'config'], function () {
                /* 获取配置 */
                Route::post('browse', 'ConfigController@browse');
                /* 编辑 */
                Route::post('edit', 'ConfigController@edit');
            });

            /* 操作日志 */
            Route::group(['prefix' => 'log'], function () {
                /* 浏览日志 */
                Route::post('browse', 'AdminOperationLogController@browse');
            });
        });

        /* 开发者工具 */
        Route::group(['prefix' => 'dev'], function () {

            /* 后台菜单管理 */
            Route::group(['prefix' => 'adminRoute'], function () {
                /* 获取列表 */
                Route::post('browse', 'AdminRouteController@browse');
                /* 添加 */
                Route::post('add', 'AdminRouteController@add');
                /* 编辑 */
                Route::post('edit', 'AdminRouteController@edit');
                /* 删除 */
                Route::post('remove', 'AdminRouteController@remove');
                /* 设置方法 */
                Route::post('setPermission', 'AdminRouteController@setPermission');
                /* 获取方法列表 */
                Route::post('getPermissionList', 'AdminRouteController@getPermissionList');
            });

            /* Redis 管理*/
            Route::group(['prefix' => 'redis'], function () {
                /* 获取key列表 */
                Route::post('getKeyList', 'RedisController@getKeyList');
                /* 获取列表 */
                Route::post('browse', 'RedisController@browse');
                /* 详情 */
                Route::post('info', 'RedisController@info');
                /* 编辑 */
                Route::post('edit', 'RedisController@edit');
                /* 删除 */
                Route::post('remove', 'RedisController@remove');
                /* 设置过期时间 */
                Route::post('ttl', 'RedisController@ttl');
            });
        });

        Route::group(['prefix' => 'common'], function () {
            /* 上传图片 */
            Route::post('uploadImage', 'CommonController@uploadImage');
            /* 上传文件 */
            Route::post('uploadFile', 'CommonController@uploadFile');
            /* 获取权限组选项 */
            Route::post('getAdminRoleOptions', 'CommonController@getAdminRoleOptions');
            /* 获取后台用户选项 */
            Route::post('getAdminOptions', 'CommonController@getAdminOptions');
            /* 获取后台顶级菜单选项选项 */
            Route::post('getAdminRouteOptions', 'CommonController@getAdminRouteOptions');
        });

        Route::group(['prefix' => 'my'], function () {
            /* 设置个人信息 */
            Route::post('setInfo', 'MyController@setInfo');
            /* 获取通知消息 */
            Route::post('getNoticeList', 'MyController@getNoticeList');
            /* 标记消息已读 */
            Route::post('readNotice', 'MyController@readNotice');
        });

        Route::group(['prefix' => 'dashboard'], function () {
            Route::group(['prefix' => 'todo'], function () {
                Route::post('browse', 'TodoController@browse');

                Route::post('add', 'TodoController@add');

                Route::post('edit', 'TodoController@edit');

                Route::post('remove', 'TodoController@remove');
            });
        });
    });

});

/* 下载文件 */
Route::get('/common/file/{imageID}', 'CommonController@file');

